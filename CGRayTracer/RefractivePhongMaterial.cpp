//
//  RefractiveMaterial.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/13/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "RefractivePhongMaterial.h"
#include "RayTracer.h"
#include "constants.h"

RefractivePhongMaterial::RefractivePhongMaterial(Vector3 ambientC,
                                                 Vector3 diffuseC,
                                                 Vector3 specularC,
                                                 double specularExp,
                                                 double refractiveIndex):
PhongMaterial(ambientC, diffuseC, specularC, specularExp),
refractiveIndex_(refractiveIndex)
{
    
}

Vector3 RefractivePhongMaterial::shade(const IntersectionPoint & p,
                                       const Scene & scene, int recursionsToGo)
{
    
    
    Vector3 phongC = PhongMaterial::shade(p, scene, recursionsToGo);
    
    // Check if we are inside or outside
    bool from_inside = p.incident.dot(p.normal) > 0;
    
    Vector3 N;
    double n;
    if (from_inside) {
        N = -p.normal;
        n = refractiveIndex_;
    }else{
        N = p.normal;
        n = 1.0f/refractiveIndex_;
    }
    
    assert(0.9f < p.incident.length() && p.incident.length() < 1.1f);
    
    // Compute refraction direction
    double cos_ph1 = -(p.incident).dot(N);
    double sin2 = (n*n) * (1 - (cos_ph1 * cos_ph1));
        
    Vector3 refractionDir = p.incident * n + N * ( n*cos_ph1 - sqrt(1.0f - sin2));
    
    Ray refr = Ray(p.hitPoint, refractionDir);
    Vector3 refractionColor = (sin2 > 1.0f) ? Vector3(0,0,0) : RayTracer::shadeRay(refr, scene, recursionsToGo);
    
    
    Vector3 reflectionDir = p.incident + N *
        (-p.incident).dot(N) * 2;
    Ray r = Ray(p.hitPoint, reflectionDir);
    Vector3 reflectionColor = RayTracer::shadeRay(r, scene, recursionsToGo);
    
    if(sin2 > 1.0f)
        return phongC * 0.5f + reflectionColor;
   
      
    return phongC * 0.5f + (reflectionColor + refractionColor) * 0.5f;
   
}