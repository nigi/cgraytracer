//
//  RayTracer.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/12/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "RayTracer.h"
#include "constants.h"
#include <limits>

RayTracer::RayTracer() :
raysPerPixelSqrt_(1)
{
    
}


void RayTracer::traceScene(Scene &scene, float *g_buffer, unsigned int g_width,
                           unsigned int g_height, void (*update)(int,int))
{
    int raysPerPixel = raysPerPixelSqrt_ * raysPerPixelSqrt_;
    
    double delta_u = (1.0f/g_width) / raysPerPixelSqrt_;
    double delta_v = (1.0f/g_height) / raysPerPixelSqrt_;
    
    for (int y = 0; y < g_height; y++) {
		for (int x = 0; x < g_width; x++) {
            
            int pos = (y * g_width + x) * 3;
            
            for(int raysx = 0; raysx < raysPerPixelSqrt_; raysx++){
                for(int raysy = 0; raysy < raysPerPixelSqrt_; raysy++){
                    
                    double u = x * delta_u * raysPerPixelSqrt_ +
                        (0.5f+raysx)* delta_u;
                    double v = y * delta_v * raysPerPixelSqrt_ +
                        (0.5f+raysy)* delta_v;
                    
                    // Generate ray through pixel with camera
                    // Ray r = scene.camera().generateRayForPixel(x, y);
                    Ray r = scene.camera()->generateRayForUV(u, v);
                    Vector3 color = shadeRay(r, scene, MAX_RECURSION_DEPTH);
                    
                    g_buffer[pos] += (float)color.x;
                    g_buffer[pos + 1] += (float)color.y;
                    g_buffer[pos + 2] += (float)color.z;
                    
                }
            }
            
            g_buffer[pos] /= raysPerPixel;
            g_buffer[pos + 1] /= raysPerPixel;
            g_buffer[pos + 2] /= raysPerPixel;
            
            // update the window
            update(y * g_width + x + 1, g_width * g_height);
		}
	}
}

// Shoots a ray through the scene and lets the material shade the
// intersection-point.
Vector3 RayTracer::shadeRay(Ray &r, const Scene &scene, int recursionToGo)
{
    if (recursionToGo--<0) {
        return Vector3(0.0f,0.0f,0.0f);
    }
    
    // Intersect ray with objects in scene
    IntersectionPoint inter;
    double mint = (std::numeric_limits< double >::max)();
    r.o() = r.o() + r.d() * INTERSECTION_OFFSET;
    
    for(int i = 0; i < scene.objects().size(); i++)
    {
        SceneObject * o = scene.objects().at(i);
        IntersectionPoint tinter = o->intersect(r);
        
        if (tinter.hit && tinter.t < mint && tinter.t > 0)
        {
            mint = tinter.t;
            inter = tinter;
        }
    }
    
    // Write color back into buffer
    if(inter.hit)
    {
        Material * mat = inter.material;
        return mat->shade(inter, scene, recursionToGo);
    }
    
    return Vector3(0,0,0);
}

// Shoots ray through scene and returns if the point is shadowed by other
// objects or not
bool RayTracer::pointShadowed(Ray &r, double lightDist, const Scene &scene)
{
    r.o() = r.o() + r.d() * INTERSECTION_OFFSET;

    for(int i = 0; i < scene.objects().size(); i++)
    {
        SceneObject * o = scene.objects().at(i);
        IntersectionPoint tinter = o->intersect(r);
        
        if (tinter.hit && tinter.t < lightDist &&
            tinter.t > 0)
        {
            return true;
        }
    }
    
    return false;
}

//----------------------------------------------------------------------------//
//  Setters & Getters
//----------------------------------------------------------------------------//

void RayTracer::setRayNumber(int raysPerPixel)
{
    raysPerPixelSqrt_ = (int)sqrt((double)raysPerPixel);
}
