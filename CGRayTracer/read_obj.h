#pragma once

// Function to read a mesh from a .obj file
//

#include <stdio.h>
#include <vector>
#include "utils.h"

// a simple type for vertex indices
typedef std::vector<size_t> Face;

// Read a mesh from a .obj file, output a list of vertex positions, face indices
//   and texture coordinates (if given)
// Inputs:
//   obj_file_name  path to .obj file
// Outputs:
//   V  #V list of vertex positions ("Vector3"s, each containing three elements (x,y,z))
//   TC  #TC list of texture coordinates ("Vectors2"s, each containing 2 elements
//   F  #F list of face corner vertex index lists, C++ vectors indexing V
//   FTC  #F list of face corner TC index lists, C++ vectors indexing TC
// Return
//   true only if no error occurred

inline bool read_obj(
  const char* obj_file_name,
  std::vector<Vector3> & V,
  std::vector<Vector2> & TC,
  std::vector<Face> & F,
  std::vector<Face> & FTC)
{
  // Open file, and check for error
  FILE * obj_file = fopen(obj_file_name,"r");                                       
  if(NULL==obj_file)
  {
    fprintf(stderr,"IOError: %s could not be opened...\n",obj_file_name);
    return false; 
  }

  // File open was succesfull so clear outputs
  V.clear();
  TC.clear();
  F.clear();
  FTC.clear();

  // variables an constants to assist parsing the .obj file
  // flag for whether vertex texture coordinates exist in file
  bool has_texture = false;
  // flag for whether vertex normals exist in file
  bool has_normals = false;
  // Constant strings to compare against
  std::string v("v");
  std::string vn("vn");
  std::string vt("vt");
  std::string f("f");
  std::string tic_tac_toe("#");
  // variable used to determine what type of line currently being processed
  char type[1024];
  while(fscanf(obj_file, "%s",type)==1)
  {
    if(type == v)
    {
      float x,y,z;
      fscanf(obj_file, "%g %g %g\n",&x,&y,&z);
      Vector3 vertex;
      vertex[0] = x;
      vertex[1] = y;
      vertex[2] = z;
      V.push_back(vertex);
    }else if(type == vn)
    {
      has_normals = true;
      float x,y,z;
      fscanf(obj_file, "%g %g %g\n",&x,&y,&z);      
    }else if(type == vt)
    {
      has_texture = true;
      float x,y;
      fscanf(obj_file, "%g %g\n",&x,&y);      
      Vector2 texture_coordinate;
      texture_coordinate[0] = x;
      texture_coordinate[1] = y;
      TC.push_back(texture_coordinate);
    }else if(type == f)
    {
      Face face;
      // use unsigned int here so compiling for 64-bits doesn't produce warning
      // on %u in fscanf
      unsigned int i,it,in;
      Face face_tc;
      if(has_texture & has_normals)
      {
        while(fscanf(obj_file,"%u/%u/%u",&i,&it,&in)==3)
        {
          face.push_back(i-1);
          face_tc.push_back(it-1);
        }
      }
      else if(has_texture)
      {
        while(fscanf(obj_file,"%u/%u",&i,&it)==2)
        {
          face.push_back(i-1);
          face_tc.push_back(it-1);
        }
      }
      else if(has_normals)
      {
        while(fscanf(obj_file,"%u//%u",&i,&in)==2)
        {
          face.push_back(i-1);
        }
      }
      else
      {
        while(fscanf(obj_file,"%u",&i)==1)
        {
          face.push_back(i-1);
        }
      }
      F.push_back(face);
      if(face_tc.size() != 0)
      {
        FTC.push_back(face_tc);
      }
    }else if(type == tic_tac_toe)
    {
      //ignore comments
      char comment[1000];
      fscanf(obj_file,"%[^\n]",comment);
    }else
    {
      //ignore any other lines
      char comment[1000];
      fscanf(obj_file,"%[^\n]",comment);
    }
  }
  return true;
}


// we're not interested in texture coordinates here
//
inline bool read_obj(
  const char* obj_file_name,
  std::vector<Vector3> & V,
  std::vector<Face> & F)
{
  // we don't take these guys as arguments, as we don't use them
  std::vector<Vector2> TC;
  std::vector<Face> FTC;

  return read_obj(obj_file_name, V, TC, F, FTC);
}

