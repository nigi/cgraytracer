//
//  DiskLight.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 11/8/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__DiskLight__
#define __CGRayTracer__DiskLight__

#include <iostream>
#include "Light.h"
#include "SceneObject.h"
#include "LightIntensities.h"
#include "LightMaterial.h"
#include <vector>

using namespace std;

class DiskLight : public Light, public SceneObject{
    
    Vector3 center_;
    Vector3 orientation_;
    Vector3 v1_;
    Vector3 v2_;
    
    double radius_;
    
    int samples_;
    
    Vector3 color_;
    
    double ambientI;
    double diffuseI;
    double specularI;
    
    LightMaterial * lmat_;
    
public:
    DiskLight(Vector3 center, Vector3 orientation, double radius,
              Vector3 color, double ambient, double diffuse, double specular);
    void setNumberOfSamples(int s);
    vector<LightIntensities> getIntensities(Vector3 worldpos);
    IntersectionPoint intersect(Ray &r);
};

#endif /* defined(__CGRayTracer__DiskLight__) */
