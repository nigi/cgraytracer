/*
 *  utils.h
 *  raytracer
 *  Computer Graphics course exercise
 *
 *  Thomas Oskam, Michael Eigensatz, Hao Li, Balint Miklos, Raphael Hoever, Steven Poulakos, Marcel Germann
 *  Copyright 2011 ETH Zürich. All rights reserved.
 *
 */

// includes vector and matrix math helpers:
//
// Vector2, Vector3, Vector4 - 2, 3, and 4-vectors and some operations
// Matrix4 - 4x4 matrix and some operations


#pragma once


#include <math.h>
#include <assert.h>
#include <iostream>

#ifndef M_PI
    #define M_PI 3.14159265f
#endif


class Vector2 {
public:
	// Vector2 Methods
	Vector2(double _x=0, double _y=0)
	: x(_x), y(_y) {
	}
	
	Vector2 operator+(const Vector2 &v) const {
		return Vector2(x + v.x, y + v.y);
	}
	
	Vector2& operator+=(const Vector2 &v) {
		x += v.x; y += v.y;
		return *this;
	}
	
	Vector2 operator-(const Vector2 &v) const {
		return Vector2(x - v.x, y - v.y);
	}
	
	Vector2& operator-=(const Vector2 &v) {
		x -= v.x; y -= v.y;
		return *this;
	}
	
	bool operator==(const Vector2 &v) const {
		return x == v.x && y == v.y;
	}
	
	Vector2 operator*(double f) const {
		return Vector2(f*x, f*y);
	}
	
	Vector2 &operator*=(double f) {
		x *= f; y *= f;
		return *this;
	}
	
	Vector2 operator/(double f) const {
		assert(f!=0);
		double inv = 1.f / f;
		return Vector2(x * inv, y * inv);
	}
	
	Vector2 &operator/=(double f) {
		assert(f!=0);
		double inv = 1.f / f;
		x *= inv; y *= inv;
		return *this;
	}
	
	Vector2 operator-() const {
		return Vector2(-x, -y);
	}
	
	double &operator[](int i) {
		assert(i >= 0 && i <= 1);
		switch (i) {
			case 0: return x;
			case 1: return y;
		}
	} 
	
	Vector2 normalize() { 
		assert(length() > 0);
		double l = length();
		x /= l;
		y /= l;
		return *this;
	}
	
	Vector2 clamp01() { 
		if (x>1.f) x=1.f;
		else if (x<0.f) x=0.f;
		if (y>1.f) y=1.f;
		else if (y<0.f) y=0.f;
		return *this;
	}
	
	
	double lengthSquared() const { return x*x + y*y; }
	
	double length() const { return sqrt(lengthSquared()); }
	
	// Vector2 Data
	double x, y;
};


class Vector3 {
public:
	// Vector3 Methods
	Vector3(double _x=0, double _y=0, double _z=0)
	: x(_x), y(_y), z(_z) {
	}
    
    
	
	Vector3 operator+(const Vector3 &v) const {
		return Vector3(x + v.x, y + v.y, z + v.z);
	}
	
	Vector3& operator+=(const Vector3 &v) {
		x += v.x; y += v.y; z += v.z;
		return *this;
	}
	
	Vector3 operator-(const Vector3 &v) const {
		return Vector3(x - v.x, y - v.y, z - v.z);
	}
	
	Vector3& operator-=(const Vector3 &v) {
		x -= v.x; y -= v.y; z -= v.z;
		return *this;
	}
	
	bool operator==(const Vector3 &v) const {
		return x == v.x && y == v.y && z == v.z;
	}
    
    Vector3 operator/(const Vector3 &v) const {
		return Vector3(x/v.x, y/v.y, z/v.z);
	}
    
    Vector3& operator/=(const Vector3 &v) {
        x/=v.x; y/=v.y; z/=v.z;
		return *this;
	}
    
    Vector3 operator*(const Vector3 &v) const {
		return Vector3(x*v.x, y*v.y, z*v.z);
	}
    
    Vector3& operator*=(const Vector3 &v) {
        x*=v.x; y*=v.y; z*=v.z;
		return *this;
	}
	
	Vector3 operator*(double f) const {
		return Vector3(f*x, f*y, f*z);
	}
	
	Vector3 &operator*=(double f) {
		x *= f; y *= f; z *= f;
		return *this;
	}
	
	Vector3 operator/(double f) const {
		assert(f!=0);
		double inv = 1.f / f;
		return Vector3(x * inv, y * inv, z * inv);
	}
	
	Vector3 &operator/=(double f) {
		assert(f!=0);
		double inv = 1.f / f;
		x *= inv; y *= inv; z *= inv;
		return *this;
	}
	
	Vector3 operator-() const {
		return Vector3(-x, -y, -z);
	}
	
	double &operator[](int i) {
		assert(i >= 0 && i <= 2);
		switch (i) {
			case 0: return x;
			case 1: return y;
			case 2: return z;
		}
		return z;
	} 
	
	const double &operator[](int i) const {
		assert(i >= 0 && i <= 2);
		switch (i) {
			case 0: return x;
			case 1: return y;
			case 2: return z;
			default: return z;
		}
	}
	
	
	Vector3 cross(const Vector3 &v) const{
		return Vector3(y*v.z - z*v.y, z*v.x - x*v.z, x*v.y - y * v.x);
	}
	
	double dot(const Vector3 &v) const{
		return x*v.x + y*v.y + z*v.z;
	}
	
	Vector3 normalize() { 
		assert(length() != 0);
		double l = length();
		x /= l;
		y /= l;
		z /= l;
		return *this;
	}
	
	Vector3 clamp01() { 
		if (x>1.f) x=1.f;
		else if (x<0.f) x=0.f;
		if (y>1.f) y=1.f;
		else if (y<0.f) y=0.f;
		if (z>1.f) z=1.f;
		else if (z<0.f) z=0.f;
		return *this;
	}
	
	double lengthSquared() const { return x*x + y*y + z*z; }
	
	double length() const { return sqrt(lengthSquared()); }
	
	// Vector3 Data
	double x, y, z;
};


class Vector4 {
public:
	// Vector4 Methods
	Vector4(double _x=0, double _y=0, double _z=0, double _w=0)
	: x(_x), y(_y), z(_z), w(_w) {
	}
	
	Vector4 operator+(const Vector4 &v) const {
		return Vector4(x + v.x, y + v.y, z + v.z, w + v.w);
	}
	
	Vector4& operator+=(const Vector4 &v) {
		x += v.x; y += v.y; z += v.z; w += v.w;
		return *this;
	}
	
	Vector4 operator-(const Vector4 &v) const {
		return Vector4(x - v.x, y - v.y, z - v.z, w - v.w);
	}
	
	Vector4& operator-=(const Vector4 &v) {
		x -= v.x; y -= v.y; z -= v.z, w -=v.w;
		return *this;
	}
	
	bool operator==(const Vector4 &v) const {
		return x == v.x && y == v.y && z == v.z && w == v.w;
	}
	
	Vector4 operator*(double f) const {
		return Vector4(f*x, f*y, f*z, f*w);
	}
	
	Vector4 &operator*=(double f) {
		x *= f; y *= f; z *= f; w *= f;
		return *this;
	}
	
	Vector4 operator/(double f) const {
		assert(f!=0);
		double inv = 1.f / f;
		return Vector4(x * inv, y * inv, z * inv, w * inv);
	}
	
	Vector4 &operator/=(double f) {
		assert(f!=0);
		double inv = 1.f / f;
		x *= inv; y *= inv; z *= inv; w *= inv;
		return *this;
	}
	
	Vector4 operator-() const {
		return Vector4(-x, -y, -z, -w);
	}
	
	double &operator[](int i) {
		assert(i >= 0 && i <= 3);
		switch (i) {
			case 0: return x;
			case 1: return y;
			case 2: return z;
			case 3: return w;
		}
	} 
	
	double dot(const Vector4 &v) const {
		return x*v.x + y*v.y + z*v.z + w*v.w;
	}
	
	Vector4 componentMul(const Vector4 &v) const {
		return Vector4(x*v.x, y*v.y, z*v.z, w*v.w);
	}
	
	Vector4 clamp01() { 
		if (x>1.f) x=1.f;
		else if (x<0.f) x=0.f;
		if (y>1.f) y=1.f;
		else if (y<0.f) y=0.f;
		if (z>1.f) z=1.f;
		else if (z<0.f) z=0.f;
		if (w>1.f) w=1.f;
		else if (w<0.f) w=0.f;
		return *this;
	}
	
	double lengthSquared() const { return x*x + y*y + z*z + w*w; }
	
	double length() const { return sqrt(lengthSquared()); }
	
	// Vector4 Data
	double x, y, z, w;
};

class Matrix3 {
    
    
public:
    Matrix3(Vector3 a, Vector3 b, Vector3 c)
    {
        m[0][0] = a.x; m[0][1] = b.x; m[0][2] = c.x;
        m[1][0] = a.y; m[1][1] = b.y; m[1][2] = c.y;
        m[2][0] = a.z; m[2][1] = b.z; m[2][2] = c.z;
    }
    
    double det()
    {
        return
          m[0][0]*m[1][1]*m[2][2]
        + m[0][1]*m[1][2]*m[2][0]
        + m[0][2]*m[1][0]*m[2][1]
        
        - m[0][2]*m[1][1]*m[2][0]
        - m[1][2]*m[2][1]*m[0][0]
        - m[2][2]*m[0][1]*m[1][0];
    }
    
    Matrix3(const Matrix3& a){
        for (int i = 0; i < 3 ; i++) {
            for (int j = 0; j < 3; j++) {
                m[i][j] = a.m[i][j];
            }
        }
    }
    
    void columnSet(int i, Vector3 a)
    {
        //Matrix3 r(Vector3(0,0,0),Vector3(0,0,0),Vector3(0,0,0));
        m[i][0] = a.x;
        m[i][1] = a.y;
        m[i][2] = a.z;
        //return r;
    }
    
    double m[3][3];
};


class Matrix4 {
public:
	// Matrix4 Constructors
	////////////////////////
	
	Matrix4(double m_00=0, double m_01=0, double m_02=0, double m_03=0,
	        double m_10=0, double m_11=0, double m_12=0, double m_13=0,
			double m_20=0, double m_21=0, double m_22=0, double m_23=0,
			double m_30=0, double m_31=0, double m_32=0, double m_33=0) {
		m[0][0] = m_00; m[0][1] = m_01; m[0][2] = m_02;  m[0][3] = m_03;
		m[1][0] = m_10; m[1][1] = m_11; m[1][2] = m_12;  m[1][3] = m_13;
		m[2][0] = m_20; m[2][1] = m_21; m[2][2] = m_22;  m[2][3] = m_23;
		m[3][0] = m_30; m[3][1] = m_31; m[3][2] = m_32;  m[3][3] = m_33;
	}
	
	Matrix4(double n[4][4]) {
		m[0][0]=n[0][0]; m[0][1]=n[0][1]; m[0][2]=n[0][2]; m[0][3]=n[0][3];
		m[1][0]=n[1][0]; m[1][1]=n[1][1]; m[1][2]=n[1][2]; m[1][3]=n[1][3];
		m[2][0]=n[2][0]; m[2][1]=n[2][1]; m[2][2]=n[2][2]; m[2][3]=n[2][3];
		m[3][0]=n[3][0]; m[3][1]=n[3][1]; m[3][2]=n[3][2]; m[3][3]=n[3][3];
	}
	
	Matrix4(const Vector3 a1, const Vector3 a2, const Vector3 a3) {
		m[0][0] = a1.x; m[0][1] = a2.x; m[0][2] = a3.x;  m[0][3] = 0.f;
		m[1][0] = a1.y; m[1][1] = a2.y; m[1][2] = a3.y;  m[1][3] = 0.f;
		m[2][0] = a1.z; m[2][1] = a2.z; m[2][2] = a3.z;  m[2][3] = 0.f;
		m[3][0] = 0.f;  m[3][1] = 0.f;  m[3][2] = 0.f;   m[3][3] = 1.f;
	}
	
	
	Matrix4(const Vector4 a1, const Vector4 a2, const Vector4 a3) {
		m[0][0] = a1.x; m[0][1] = a2.x; m[0][2] = a3.x;  m[0][3] = 0.f;
		m[1][0] = a1.y; m[1][1] = a2.y; m[1][2] = a3.y;  m[1][3] = 0.f;
		m[2][0] = a1.z; m[2][1] = a2.z; m[2][2] = a3.z;  m[2][3] = 0.f;
		m[3][0] = a1.w; m[3][1] = a2.w; m[3][2] = a3.w;  m[3][3] = 1.f;
	}
	
	
	// Matrix4-Matrix4 Operations
	/////////////////////////////
	
	Matrix4 operator+(const Matrix4 &n) const {
		return Matrix4(m[0][0]+n.m[0][0], m[0][1]+n.m[0][1], m[0][2]+n.m[0][2], m[0][3]+n.m[0][3],
		               m[1][0]+n.m[1][0], m[1][1]+n.m[1][1], m[1][2]+n.m[1][2], m[1][3]+n.m[1][3],
		               m[2][0]+n.m[2][0], m[2][1]+n.m[2][1], m[2][2]+n.m[2][2], m[2][3]+n.m[2][3],
		               m[3][0]+n.m[3][0], m[3][1]+n.m[3][1], m[3][2]+n.m[3][2], m[3][3]+n.m[3][3]);
	}
	
	Matrix4& operator+=(const Matrix4 &n) {
		m[0][0]+=n.m[0][0]; m[0][1]+=n.m[0][1]; m[0][2]+=n.m[0][2]; m[0][3]+=n.m[0][3];
		m[1][0]+=n.m[1][0]; m[1][1]+=n.m[1][1]; m[1][2]+=n.m[1][2]; m[1][3]+=n.m[1][3];
		m[2][0]+=n.m[2][0]; m[2][1]+=n.m[2][1]; m[2][2]+=n.m[2][2]; m[2][3]+=n.m[2][3];
		m[3][0]+=n.m[3][0]; m[3][1]+=n.m[3][1]; m[3][2]+=n.m[3][2]; m[3][3]+=n.m[3][3];
		return *this;
	}
	
	Matrix4 operator-(const Matrix4 &n) const {
		return Matrix4(m[0][0]-n.m[0][0], m[0][1]-n.m[0][1], m[0][2]-n.m[0][2], m[0][3]-n.m[0][3],
		               m[1][0]-n.m[1][0], m[1][1]-n.m[1][1], m[1][2]-n.m[1][2], m[1][3]-n.m[1][3],
		               m[2][0]-n.m[2][0], m[2][1]-n.m[2][1], m[2][2]-n.m[2][2], m[2][3]-n.m[2][3],
		               m[3][0]-n.m[3][0], m[3][1]-n.m[3][1], m[3][2]-n.m[3][2], m[3][3]-n.m[3][3]);
	}
	
	Matrix4& operator-=(const Matrix4 &n) {
		m[0][0]-=n.m[0][0]; m[0][1]-=n.m[0][1]; m[0][2]-=n.m[0][2]; m[0][3]-=n.m[0][3];
		m[1][0]-=n.m[1][0]; m[1][1]-=n.m[1][1]; m[1][2]-=n.m[1][2]; m[1][3]-=n.m[1][3];
		m[2][0]-=n.m[2][0]; m[2][1]-=n.m[2][1]; m[2][2]-=n.m[2][2]; m[2][3]-=n.m[2][3];
		m[3][0]-=n.m[3][0]; m[3][1]-=n.m[3][1]; m[3][2]-=n.m[3][2]; m[3][3]-=n.m[3][3];
		return *this;
	}
	
	bool operator==(const Matrix4 &n) const {
		return m[0][0]==n.m[0][0] && m[0][1]==n.m[0][1] && m[0][2]==n.m[0][2] && m[0][3]==n.m[0][3] &&
		m[1][0]==n.m[1][0] && m[1][1]==n.m[1][1] && m[1][2]==n.m[1][2] && m[1][3]==n.m[1][3] &&
		m[2][0]==n.m[2][0] && m[2][1]==n.m[2][1] && m[2][2]==n.m[2][2] && m[2][3]==n.m[2][3] &&
		m[3][0]==n.m[3][0] && m[3][1]==n.m[3][1] && m[3][2]==n.m[3][2] && m[3][3]==n.m[3][3];
	}
	
	
	// Matrix4-Vector operations
	////////////////////////////
	
	Vector3 operator*(const Vector3 &v) const {
		Vector3 u =Vector3(m[0][0]*v.x + m[0][1]*v.y + m[0][2]*v.z + m[0][3],
		                   m[1][0]*v.x + m[1][1]*v.y + m[1][2]*v.z + m[1][3],
						   m[2][0]*v.x + m[2][1]*v.y + m[2][2]*v.z + m[2][3]);
		double w = m[3][0]*v.x + m[3][1]*v.y + m[3][2]*v.z + m[3][3];
		return u/w;
	}
	
	
	Vector4 operator*(const Vector4 &v) const {
		Vector4 u =Vector4(m[0][0]*v.x + m[0][1]*v.y + m[0][2]*v.z + v.w*m[0][3],
		                   m[1][0]*v.x + m[1][1]*v.y + m[1][2]*v.z + v.w*m[1][3],
						   m[2][0]*v.x + m[2][1]*v.y + m[2][2]*v.z + v.w*m[2][3], 1.f);
		double w = m[3][0]*v.x + m[3][1]*v.y + m[3][2]*v.z + v.w*m[3][3];
		return u/w;
	}
	
	
	// Matrix4-double operations
	///////////////////////////
	
	Matrix4 operator*(double f) const {
		return Matrix4(m[0][0]*f, m[0][1]*f, m[0][2]*f,  m[0][3]*f,
		               m[1][0]*f, m[1][1]*f, m[1][2]*f,  m[1][3]*f,
		               m[2][0]*f, m[2][1]*f, m[2][2]*f,  m[2][3]*f,
		               m[3][0]*f, m[3][1]*f, m[3][2]*f,  m[3][3]*f);
	}
	
	Matrix4 &operator*=(double f) {
		m[0][0]*=f; m[0][1]*=f; m[0][2]*=f;  m[0][3]*=f;
		m[1][0]*=f; m[1][1]*=f; m[1][2]*=f;  m[1][3]*=f;
		m[2][0]*=f; m[2][1]*=f; m[2][2]*=f;  m[2][3]*=f;
		m[3][0]*=f; m[3][1]*=f; m[3][2]*=f;  m[3][3]*=f;
		return *this;
	}
	
	Matrix4 operator/(double f) const {
		assert(f!=0);
		return Matrix4(m[0][0]/f, m[0][1]/f, m[0][2]/f,  m[0][3]/f,
		               m[1][0]/f, m[1][1]/f, m[1][2]/f,  m[1][3]/f,
		               m[2][0]/f, m[2][1]/f, m[2][2]/f,  m[2][3]/f,
		               m[3][0]/f, m[3][1]/f, m[3][2]/f,  m[3][3]/f);
	}
	
	Matrix4 &operator/=(double f) {
		assert(f!=0);
		m[0][0]/=f; m[0][1]/=f; m[0][2]/=f;  m[0][3]/=f;
		m[1][0]/=f; m[1][1]/=f; m[1][2]/=f;  m[1][3]/=f;
		m[2][0]/=f; m[2][1]/=f; m[2][2]/=f;  m[2][3]/=f;
		m[3][0]/=f; m[3][1]/=f; m[3][2]/=f;  m[3][3]/=f;
		return *this;
	}
	
	
	// Matrix4 self operations
	//////////////////////////
	
	Matrix4 operator-() const {
		return Matrix4(-m[0][0], -m[0][1], -m[0][2],  -m[0][3],
		               -m[1][0], -m[1][1], -m[1][2],  -m[1][3],
		               -m[2][0], -m[2][1], -m[2][2],  -m[2][3],
		               -m[3][0], -m[3][1], -m[3][2],  -m[3][3]);
	}
	
	Matrix4 Transpose() const {
		return Matrix4(m[0][0], m[1][0], m[2][0], m[3][0],
					   m[0][1], m[1][1], m[2][1], m[3][1],
					   m[0][2], m[1][2], m[2][2], m[3][2],
					   m[0][3], m[1][3], m[2][3], m[3][3]);
	}
	
	Matrix4 Inverse() const {
		int indxc[4], indxr[4];
		int ipiv[4] = { 0, 0, 0, 0 };
		double minv[4][4];
		double temp;
		
		for (int s=0; s<4; s++) {
			for (int t=0; t<4; t++) {
				minv[s][t] = m[s][t];
			}
		}
		
		for (int i = 0; i < 4; i++) {
			int irow = -1, icol = -1;
			double big = 0.;
			// Choose pivot
			for (int j = 0; j < 4; j++) {
				if (ipiv[j] != 1) {
					for (int k = 0; k < 4; k++) {
						if (ipiv[k] == 0) {
							if (fabs(minv[j][k]) >= big) {
								big = double(fabs(minv[j][k]));
								irow = j;
								icol = k;
							}
						}
						else if (ipiv[k] > 1) {
							std::cout << "ERROR: Singular matrix in MatrixInvert\n";
						}
					}
				}
			}
			++ipiv[icol];
			// Swap rows _irow_ and _icol_ for pivot
			if (irow != icol) {
				for (int k = 0; k < 4; ++k){
					temp = minv[irow][k];
					minv[irow][k] = minv[icol][k];
					minv[icol][k] = temp;
				}
			}
			indxr[i] = irow;
			indxc[i] = icol;
			if (minv[icol][icol] == 0.){
				std::cout << "Singular matrix in MatrixInvert\n";
			}
			// Set $m[icol][icol]$ to one by scaling row _icol_ appropriately
			double pivinv = 1.f / minv[icol][icol];
			minv[icol][icol] = 1.f;
			for (int j = 0; j < 4; j++) {
				minv[icol][j] *= pivinv;
			}
			
			// Subtract this row from others to zero out their columns
			for (int j = 0; j < 4; j++) {
				if (j != icol) {
					double save = minv[j][icol];
					minv[j][icol] = 0;
					for (int k = 0; k < 4; k++) {
						minv[j][k] -= minv[icol][k]*save;
					}
				}
			}
		}
		// Swap columns to reflect permutation
		for (int j = 3; j >= 0; j--) {
			if (indxr[j] != indxc[j]) {
				for (int k = 0; k < 4; k++){
					temp = minv[k][indxr[j]];
					minv[k][indxr[j]] = minv[k][indxc[j]];
					minv[k][indxc[j]] = temp;
				}
			}
		}
		return Matrix4(minv);
	}
	
	Matrix4 loadIdentity() {
		m[0][0] = 1.f; m[0][1] = 0.f; m[0][2] = 0.f;  m[0][3] = 0.f;
		m[1][0] = 0.f; m[1][1] = 1.f; m[1][2] = 0.f;  m[1][3] = 0.f;
		m[2][0] = 0.f; m[2][1] = 0.f; m[2][2] = 1.f;  m[2][3] = 0.f;
		m[3][0] = 0.f; m[3][1] = 0.f; m[3][2] = 0.f;  m[3][3] = 1.f;
		return *this;
	}
	
	Matrix4 loadRotation(Vector3 axis, double angle) {
		double cos_a = cos(angle);
		double sin_a = sin(angle);
		
		m[0][0] = cos_a + (1-cos_a)*axis.x*axis.x; 
		m[0][1] = (1-cos_a)*axis.x*axis.y - axis.z*sin_a; 
		m[0][2] = (1-cos_a)*axis.x*axis.z + axis.y*sin_a;  
		m[0][3] = 0.f;
		
		m[1][0] = (1-cos_a)*axis.x*axis.y + axis.z*sin_a; 
		m[1][1] = cos_a + (1-cos_a)*axis.y*axis.y; 
		m[1][2] = (1-cos_a)*axis.y*axis.z - axis.x*sin_a;  
		m[1][3] = 0.f;
		
		m[2][0] = (1-cos_a)*axis.x*axis.z - axis.y*sin_a; 
		m[2][1] = (1-cos_a)*axis.y*axis.z + axis.x*sin_a; 
		m[2][2] = cos_a + (1-cos_a)*axis.z*axis.z;  
		m[2][3] = 0.f;
		
		m[3][0] = 0.f; 
		m[3][1] = 0.f; 
		m[3][2] = 0.f;  
		m[3][3] = 1.f;
		return *this;
	}
	
	void print() const {
		for (int i=0; i<4; i++) {
			for (int j=0; j<4; j++) {
				std::cout << m[i][j];
			}
			std::cout << "\n";
		}
	}
	
	
	// Vector3 Data
	double m[4][4];
};


// Axis aligned Bounding Box
class AABoundingBox {
public:
    Vector3 min;
    Vector3 max;
    Vector3 center;
    
    Vector3 corners[8];
    
    AABoundingBox()
    {
        center = Vector3(0,0,0);
        min = Vector3(0,0,0);
        max = Vector3(0,0,0);
        
        int i = 8;
        while (i--) {
            corners[i] = Vector3(0,0,0);
        }
    }
    
    AABoundingBox(Vector3 a,Vector3 b,Vector3 c,Vector3 d)
    {
        min.x = std::min(std::min(a.x,b.x),std::min(c.x,d.x));
        min.y = std::min(std::min(a.y,b.y),std::min(c.y,d.y));
        min.z = std::min(std::min(a.z,b.z),std::min(c.z,d.z));
        
        max.x = std::max(std::max(a.x,b.x),std::max(c.x,d.x));
        max.y = std::max(std::max(a.y,b.y),std::max(c.y,d.y));
        max.z = std::max(std::max(a.z,b.z),std::max(c.z,d.z));
        
        center = min + (max - min) * 0.5f;
        
        updateCorners();
        
    }
    
    AABoundingBox(Vector3 a,Vector3 b)
    {
        min.x = std::min(a.x,b.x);
        min.y = std::min(a.y,b.y);
        min.z = std::min(a.z,b.z);
        
        max.x = std::max(a.x,b.x);
        max.y = std::max(a.y,b.y);
        max.z = std::max(a.z,b.z);
        
        center = min + (max - min) * 0.5f;
        
        updateCorners();
    }
    
    void updateCorners()
    {
        assert(min.x <= max.x);
        assert(min.y <= max.y);
        assert(min.z <= max.z);
        
        corners[0] = min;
        corners[1].x = max.x; corners[1].y = min.y; corners[1].z = min.z;
        corners[2].x = min.x; corners[2].y = max.y; corners[2].z = min.z;
        corners[3].x = max.x; corners[3].y = max.y; corners[3].z = min.z;
        
        corners[4].x = min.x; corners[4].y = min.y; corners[4].z = max.z;
        corners[5].x = max.x; corners[5].y = min.y; corners[5].z = max.z;
        corners[6].x = min.x; corners[6].y = max.y; corners[6].z = max.z;
        corners[7] = max;

    }
    
    void extend(AABoundingBox box)
    {
        min.x = box.min.x < min.x ? box.min.x : min.x;
        min.y = box.min.y < min.y ? box.min.y : min.y;
        min.z = box.min.z < min.z ? box.min.z : min.z;

        max.x = box.max.x > max.x ? box.max.x : max.x;
        max.y = box.max.y > max.y ? box.max.y : max.y;
        max.z = box.max.z > max.z ? box.max.z : max.z;
               
        updateCorners();
        
        center = min + (max - min) * 0.5f;
    }
};

