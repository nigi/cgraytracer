//
//  RefractiveMaterial.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/13/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__RefractivePhongMaterial__
#define __CGRayTracer__RefractivePhongMaterial__

#include <iostream>
#include "PhongMaterial.h"


class RefractivePhongMaterial : public PhongMaterial {
    double refractiveIndex_;
    
public:
    RefractivePhongMaterial(Vector3 ambientC,Vector3 diffuseC,
                            Vector3 specularC, double specularExp,
                            double refractiveIndex);
    
    virtual Vector3 shade(const IntersectionPoint & p, const Scene & scene,int recursionsToGo);
    
};

#endif /* defined(__CGRayTracer__RefractiveMaterial__) */
