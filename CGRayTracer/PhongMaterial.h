//
//  PhongMaterial.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 9/23/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__PhongMaterial__
#define __CGRayTracer__PhongMaterial__

#include <iostream>
#include "utils.h"
#include "Material.h"

class PhongMaterial : public Material {

protected:
    
    Vector3 specularColor_;
    Vector3 diffuseColor_;
    Vector3 ambientColor_;
    
    double specularExponent_;
    
public:
    PhongMaterial(Vector3 ambientC, Vector3 diffuseC, Vector3 specularC, double specularExp):ambientColor_(ambientC), diffuseColor_(diffuseC), specularColor_(specularC), specularExponent_(specularExp){};
    virtual Vector3 shade(const IntersectionPoint & p, const Scene & scene,int recursionsToGo);

};
#endif /* defined(__CGRayTracer__PhongMaterial__) */
