//
//  Triangle.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 11/6/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__Triangle__
#define __CGRayTracer__Triangle__

#include <iostream>
#include "SceneObject.h"
#include "utils.h"

#include <vector>

using namespace std;

class Triangle : public SceneObject{

public:
    size_t indices[3];
    vector<Vector3> * vertices_;
    vector<Vector3> * normals_;
    vector<Vector2> * texcoords_;
    
    AABoundingBox bounds_;
    
public:
    Triangle(size_t v1, size_t v2, size_t v3);
    void setPointers(vector<Vector3> * vertices, vector<Vector3> * normals,
                     vector<Vector2> * texcoords);
    IntersectionPoint intersect(Ray &r);
    AABoundingBox boundingBox();
    
private:
    void fillIP(IntersectionPoint& p, Ray& r, double u, double v);
};

#endif /* defined(__CGRayTracer__Triangle__) */
