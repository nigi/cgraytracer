//
//  IntersectionPoint.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 9/23/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__IntersectionPoint__
#define __CGRayTracer__IntersectionPoint__

#include <iostream>
#include "utils.h"

class Material;
class SceneObject;

class IntersectionPoint {
    
public:
    double t;
    bool hit;
    Vector3 hitPoint;
    Vector3 normal;
    Vector3 incident;
    Material * material;
    SceneObject * object;
    
    Vector2 texCoords;
    Vector3 tangent;
    Vector3 cotangent;
    
    IntersectionPoint():hit(false){};
    
    bool operator<(const IntersectionPoint& a) const;
};


#endif /* defined(__CGRayTracer__IntersectionPoint__) */
