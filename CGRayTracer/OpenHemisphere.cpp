//
//  OpenHemisphere.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/22/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "OpenHemisphere.h"

IntersectionPoint OpenHemisphere::intersect(Ray &r)
{
    Vector3 ot = r.o() - center_;
    double A = r.d().dot(r.d());
    double B = 2 * (r.d().dot(ot));
    double C = ot.dot(ot) - radius_*radius_;
    
    IntersectionPoint result = IntersectionPoint();
    result.hit = false;
    
    double discr = B*B - 4*A*C;
    if(discr < 0)
    {
        result.hit = false;
        return result;
    }
    
    result.hit = true;
    
    double q;
    if(B < 0)
    {
        q = (-B - sqrt(discr)) / 2.0f;
    }else{
        q = (-B + sqrt(discr)) / 2.0f;
    }
    
    double t0 = q / A;
    double t1 = C / q;
    
    if (t0 > t1) {
        double tmp = t0;
        t0 = t1;
        t1 = tmp;
    }
    
    Vector3 hit0 = r.o() + r.d() * t0;
    Vector3 hit1 = r.o() + r.d() * t1;
    
    if (t1 < 0)
    {
        result.hit = false;
        return result;
    }
    
    result.incident = r.d();
    result.object = this;
    
    if (((t0 < 0) || (hit0.x < hit0.z)))
    {
        if (hit1.x < hit1.z) {
            result.hit = false;
            return result;
        }
        
        result.hit = true;
        result.t = t1;
        result.hitPoint = r.o() + r.d() * result.t;
        result.normal = -(result.hitPoint - center_).normalize();
        result.material = interiorMaterial_;
        
        return result;
    }
    
    
    result.hit = true;
    result.t = t0;
    result.hitPoint = r.o() + r.d() * result.t;
    result.normal = (result.hitPoint - center_).normalize();
    result.material = material();
    
    return result;

    
    
    
}