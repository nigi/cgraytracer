//
//  ray.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 9/23/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__ray__
#define __CGRayTracer__ray__

#include <iostream>
#include "utils.h"

class Ray {
    
   
    
public:
    Vector3 origin;
    Vector3 direction;
    
    Ray(Vector3 o, Vector3 d):origin(o),direction(d){};
    Vector3& o(){return origin;};
    Vector3& d(){return direction;};
    
};

#endif /* defined(__CGRayTracer__ray__) */
