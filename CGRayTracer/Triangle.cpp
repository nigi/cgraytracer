//
//  Triangle.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 11/6/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "Triangle.h"

Triangle::Triangle(size_t v1, size_t v2, size_t v3)
{
    indices[0] = v1;
    indices[1] = v2;
    indices[2] = v3;
    

}

IntersectionPoint Triangle::intersect(Ray &r)
{
    IntersectionPoint result;
    result.hit = false;
    Vector3 v0,v1,v2;
    v0 = vertices_->at(indices[0]);
    v1 = vertices_->at(indices[1]);
    v2 = vertices_->at(indices[2]);
    
    Vector3 E1 = v1 - v0;
    Vector3 E2 = v2 - v0;
    Vector3 T = r.o() - v0;
    
    Matrix3 A(-r.d(),E1,E2);

    Vector3 P = r.d().cross(E2);
    Vector3 Q = T.cross(E1);
    
    double _detA = A.det();
    double _detA0 = Q.dot(E2);
    double _detA1 = P.dot(T);
    double _detA2 = Q.dot(r.d());
    
    if (_detA > -0.0001f && _detA < 0.0001f) return result;
    
    double infdet = 1.0f / _detA;
    
    double t = _detA0 * infdet;
    
    if (t < 0.1) return result;
    
    double u = _detA1 * infdet;
    double v = _detA2 * infdet;
    
    if (u > 0 && u < 1.0f && v > 0 && v < 1.0f && (u+v) < 1) {
        // There was a hit inside the triangle
        result.t = t;
        fillIP(result, r, u, v);
    }
   
    return result;
}

void Triangle::setPointers(vector<Vector3> *vertices, vector<Vector3> *normals,
                           vector<Vector2> *texcoords)
{
    vertices_ = vertices;
    normals_ = normals;
    texcoords_ = texcoords;
    
    Vector3 v0,v1,v2;
    v0 = vertices_->at(indices[0]);
    v1 = vertices_->at(indices[1]);
    v2 = vertices_->at(indices[2]);
    
    bounds_ = AABoundingBox(v0, v1, v2, v2);
}

void Triangle::fillIP(IntersectionPoint &p, Ray &r, double u, double v)
{
    p.hit = true;
    p.hitPoint = r.o() + r.d() * p.t;
    
    Vector3 n0 = normals_->at(indices[0]);
    Vector3 n1 = normals_->at(indices[1]);
    Vector3 n2 = normals_->at(indices[2]);
    
    double w = 1.0f - u - v;
    
    p.normal = n0 * w + n1 * u + n2 * v;
    p.material = material();
    p.object = this;
}

AABoundingBox Triangle::boundingBox()
{
    return bounds_;
}
