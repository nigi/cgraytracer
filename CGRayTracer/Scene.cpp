//
//  Scene.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 9/23/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "Scene.h"

void Scene::addObject(SceneObject *object)
{
    objects_.push_back(object);
}

void Scene::addLight(Light *light)
{
    lights_.push_back(light);
}

const vector<SceneObject*>& Scene::objects() const
{
    return objects_;
}

const vector<Light*>& Scene::lights() const
{
    return lights_;
}

Camera * Scene::camera()
{
    return camera_;
}