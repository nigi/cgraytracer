//
//  SceneObject.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 9/23/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__SceneObject__
#define __CGRayTracer__SceneObject__

#include <iostream>

#include "utils.h"
#include "Ray.h"
#include "IntersectionPoint.h"
#include "Material.h"

class SceneObject {
    
    Material * material_;
    
public:
    SceneObject():material_(NULL){};
    virtual IntersectionPoint intersect(Ray &r) = 0;
    
    virtual AABoundingBox boundingBox(){ return AABoundingBox();};
    virtual void setMaterial(Material * mat){material_ = mat;};
    Material * material(){return material_;};
};

#endif /* defined(__CGRayTracer__SceneObject__) */
