//
//  PointLight.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 9/23/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "PointLight.h"

vector<LightIntensities> PointLight::getIntensities(Vector3 worldpos)
{
    LightIntensities l;
    
    l.ambientColor = color_ * ambientI;
    l.diffuseColor = color_ * diffuseI;
    l.specularColor = color_ * specularI;
    
    l.hitPoint = center_;
    
    l.light = this;
    
    vector<LightIntensities> ls;
    ls.push_back(l);
    
    return ls;
}