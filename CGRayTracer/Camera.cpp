//
//  Camera.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 9/23/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "Camera.h"

#include <cmath>



Camera::Camera()
: eyePos_(0,0,10),
 eyeDir_(0,0,-1),
 eyeUp_(0,1,0),
 d_(1.0f),
 fovy_(40.0f / 180.0f * M_PI),
 width_(800),
 height_(600)
{
    update();
}

Camera::Camera(Vector3 eyePosition, Vector3 eyeDirection,
Vector3 eyeUp, double d, double fovy, int width, int height)
: eyePos_(eyePosition),
 eyeDir_(eyeDirection),
 eyeUp_(eyeUp),
 d_(d),
 fovy_(fovy),
 width_(width),
 height_(height)
{
    update();
}

void Camera::moveTo(Vector3 eyePos)
{
    eyePos_ = eyePos;
    update();
}

void Camera::setCamera(Vector3 eyePosition, Vector3 eyeDirection, Vector3 eyeUp, double d, double fovy)
{
    eyePos_ = eyePosition;
    eyeDir_ = eyeDirection;
    eyeUp_ = eyeUp;
    d_ = d;
    fovy_ = fovy;
    
    update();
}

void Camera::setSize(int width, int height)
{
    std::cout << "setsize camera\n";

    width_ = width;
    height_ = height;
    
    update();
}

Ray Camera::generateRayForPixel(double x, double y)
{
    double u,v;
    u = (x + 0.5f) / width_ ;
    v = (y + 0.5f) / height_;
    return generateRayForUV(u, v);
}

Ray Camera::generateRayForUV(double u, double v)
{
    // Compute right vector in projection plane
    double x = (u - 0.5f) * w_;
    double y = (v - 0.5f) * h_;
    
    Vector3 rayDir = eyeDir_ * d_ + eyeUp_ * y + right_ * x;
    rayDir.normalize();
    
    return Ray(eyePos_,rayDir);
}

void Camera::update()
{
    std::cout << "update camera\n";
    
    double fovx =  fovy_ * (double)width_ / (double)height_;
    tanFovx_ = tan(fovx);
    tanFovy_ = tan(fovy_);
    
    h_ = tan(fovy_/2.0f) * d_ * 2.0f;
    w_ = (float)width_ / (float)height_ * h_;
    
    right_ = eyeDir_.cross(eyeUp_);

}