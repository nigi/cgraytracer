//
//  RayTracer.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/12/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__RayTracer__
#define __CGRayTracer__RayTracer__

#include <iostream>
#include "Scene.h"

class RayTracer {
    
    int raysPerPixelSqrt_;
public:
    RayTracer();
    
    virtual void traceScene(Scene& scene, float * g_buffer,
                            unsigned int g_width, unsigned int g_height,
                            void (*update)(int,int));
    
    void setRayNumber(int raysPerPixel);
    
    static Vector3 shadeRay(Ray& r, const Scene& scene, int recursionsToGo);
    static bool pointShadowed(Ray &r, double lightDist, const Scene &scene);
};

#endif /* defined(__CGRayTracer__RayTracer__) */
