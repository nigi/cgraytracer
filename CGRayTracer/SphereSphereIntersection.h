//
//  SphereSphereIntersection.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/22/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__SphereSphereIntersection__
#define __CGRayTracer__SphereSphereIntersection__

#include <iostream>
#include "SceneObject.h"
#include "Sphere.h"

class SphereSphereIntersection : public SceneObject{
    
    Sphere * sphere0_;
    Sphere * sphere1_;
    
public:
    SphereSphereIntersection(Sphere*s1, Sphere *s2):sphere0_(s1), sphere1_(s2){};
    IntersectionPoint intersect(Ray &r);
};

#endif /* defined(__CGRayTracer__SphereSphereIntersection__) */
