//
//  PhongMaterial.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 9/23/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "PhongMaterial.h"
#include "Light.h"
#include "Scene.h"
#include "RayTracer.h"

 Vector3 PhongMaterial::shade(const IntersectionPoint & p, const Scene & scene,
                              int recursionsToGo)
{
    //return p.normal;
        
    Vector3 color(0,0,0);
    
    for( int l = 0; l < scene.lights().size(); l++)
    {
        Light * light = scene.lights().at(l);
                
        vector<LightIntensities> ls = light->getIntensities(p.hitPoint);
        
        for (int i = 0; i < ls.size() ; i++) {
            LightIntensities li = ls[i];
            
            Vector3 lightIn = (li.hitPoint - p.hitPoint);
            double lightLength = lightIn.length();
            lightIn.normalize();
            
            
            Ray r(p.hitPoint,lightIn);
            bool shadowed = RayTracer::pointShadowed(r, lightLength, scene);
            
            
            Vector3 ambient, diffuse, specular;
            
            // Ambient Color
            ambient.x = ambientColor_.x * (li.ambientColor.x + scene.ambientColor.x);
            ambient.y = ambientColor_.y * (li.ambientColor.y + scene.ambientColor.y);
            ambient.z = ambientColor_.z * (li.ambientColor.z + scene.ambientColor.z);
            
            color += ambient;
            
            
            if(shadowed)
            {
                continue;
            }
            
            // Diffuse Color
            double LN = (lightIn).dot(p.normal);
            
            if(LN > 0){
                diffuse.x = diffuseColor_.x * (li.diffuseColor.x) * LN;
                diffuse.y = diffuseColor_.y * (li.diffuseColor.y) * LN;
                diffuse.z = diffuseColor_.z * (li.diffuseColor.z) * LN;
            }
            
            // Specular Color
            Vector3 R = lightIn - p.normal * 2 * (p.normal.dot(lightIn));
            double RV = R.dot(p.incident);
            
            if(LN > 0 && RV > 0){
                RV = pow(RV,specularExponent_);
                specular.x = li.specularColor.x * (specularColor_.x) * RV;
                specular.y = li.specularColor.y * (specularColor_.y) * RV;
                specular.z = li.specularColor.z * (specularColor_.z) * RV;
            }
            
            color += diffuse + specular;
        }
        color /= ls.size();
    }
    
	return color.clamp01();
}