//
//  LightIntensities.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 9/24/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef CGRayTracer_LightIntensities_h
#define CGRayTracer_LightIntensities_h

#include "utils.h"

class Light;

struct LightIntensities {
    
    Vector3 specularColor;
    Vector3 ambientColor;
    Vector3 diffuseColor;
    
    Vector3 hitPoint;
    
    Light * light;
};


#endif
