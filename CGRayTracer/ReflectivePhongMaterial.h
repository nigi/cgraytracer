//
//  ReflectivePhongMaterial.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/13/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__ReflectivePhongMaterial__
#define __CGRayTracer__ReflectivePhongMaterial__

#include <iostream>
#include "PhongMaterial.h"


class ReflectivePhongMaterial : public PhongMaterial {
    
    double refractiveIndex_; // Refractive index of the inside material
    
public:
    ReflectivePhongMaterial(Vector3 ambientC, Vector3 diffuseC,
                            Vector3 specularC, double specularExp);
                            
    Vector3 shade(const IntersectionPoint & p,
                  const Scene & scene,int recursionsToGo);
    
};

#endif /* defined(__CGRayTracer__ReflectivePhongMaterial__) */
