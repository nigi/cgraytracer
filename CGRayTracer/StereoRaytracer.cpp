//
//  StereoRaytracer.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/29/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "StereoRaytracer.h"
#include "constants.h"

void StereoRaytracer::traceScene(Scene &scene, float *g_buffer, unsigned int g_width, unsigned int g_height, void (*update)(int, int))
{
    Vector3 L(0.3, 0.59, 0.11);
    
    for (int y = 0; y < g_height; y++) {
		for (int x = 0; x < g_width; x++) {
            int pos = (y * g_width + x) * 3;
                                
            // Generate ray through pixel with camera
            std::pair<Ray, Ray> rays = cam_->generateRaysForPixel(x, y);

            Vector3 color_right = shadeRay(rays.first, scene, MAX_RECURSION_DEPTH);
            Vector3 color_left = shadeRay(rays.second, scene, MAX_RECURSION_DEPTH);

//            g_buffer[pos] += (float)color_left.x ;
//            g_buffer[pos + 1] += (float)color_left.y;
//            g_buffer[pos + 2] += (float)color_left.z;
//
//            g_buffer[pos] += (float)color_right.x ;
//            g_buffer[pos + 1] += (float)color_right.y;
//            g_buffer[pos + 2] += (float)color_right.z;
//            
//            g_buffer[pos] /= 2.0f;
//            g_buffer[pos + 1] /= 2.0f;
//            g_buffer[pos + 2] /= 2.0f;
            
            g_buffer[pos] = (float)L.dot(color_left);
            g_buffer[pos + 1] = (float)color_right.y;
            g_buffer[pos + 2] = (float)color_right.z;

            // update the window
            update(y * g_width + x + 1, g_width * g_height);
		}
	}

}
