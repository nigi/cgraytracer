//
//  Octree.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 11/8/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__Octree__
#define __CGRayTracer__Octree__

#include <iostream>
#include <vector>

#include "SceneObject.h"

// The threshold of object when a node is considered a leaf
#define OCTREE_LEAVE_THRESH 5
#define OCTREE_MAX_DEPTH 6

class Octree : public SceneObject {

    AABoundingBox bounds_;
    vector<SceneObject*> objects_; // if this is a leaf then here are
                                   // OCTREE_LEAVE_THRESH objects
                                   // otherwise it is only used before building
                                   // the tree.
    
    vector<Octree*> children_;
    bool isLeaf_;
    
        
public:
    Octree();
    IntersectionPoint intersect(Ray &r);
    AABoundingBox boundingBox();
    
    void addObject(SceneObject * object);
    void build();

private:
    void buildIn(AABoundingBox bounds,size_t depthToGo);
    void computeBoundingBox();
    int octantCode(Vector3& center, Vector3& point);
};

#endif /* defined(__CGRayTracer__Octree__) */
