//
//  IntersectionPoint.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 9/23/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "IntersectionPoint.h"

bool IntersectionPoint::operator<(const IntersectionPoint &a) const
{
    return t > 0 && (a.t > t || a.t < 0);
}