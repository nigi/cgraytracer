//
//  Quadric.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/22/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "Quadric.h"

IntersectionPoint Quadric::intersect(Ray &r)
{
    // for the formulas I studies this site
    // http://www.bmsc.washington.edu/people/merritt/graphics/quadrics.html
    
    
    double Aq = A * (r.d().x * r.d().x) +
                B * (r.d().y * r.d().y) +
                C * (r.d().z * r.d().z) +
                D * (r.d().x * r.d().y) +
                E * (r.d().x * r.d().z) +
                F * (r.d().y * r.d().z);
    
    double Bq = 2 * A * (r.o().x * r.d().x) +
                2 * B * (r.o().y * r.d().y) +
                2 * C * (r.o().z * r.d().z) +
                D * (r.o().x * r.d().y + r.o().y * r.d().x) +
                E * (r.o().x * r.d().z + r.o().z * r.d().x) +
                F * (r.o().y * r.d().z + r.d().y * r.o().z) +
                G * r.d().x +
                H * r.d().y +
                I * r.d().z;
    
    double Cq = A * (r.o().x * r.o().x) +
                B * (r.o().y * r.o().y) +
                C * (r.o().z * r.o().z) +
                D * (r.o().x * r.o().y) +
                E * (r.o().x * r.o().z) +
                F * (r.o().y * r.o().z) +
                G * (r.o().x) +
                H * (r.o().y) +
                I * (r.o().z) +
                J;
    
    IntersectionPoint result;
    result.object = this;
    result.incident = r.d();
    result.material = this->material();
    
    if (Aq == 0) {
        result.hit = true;
        result.t = -Cq/Bq;
    }else{
        double discr = Bq * Bq - 4*Aq*Cq;
        if (discr < 0.0f) {
            result.hit = false;
            return result;
        }
        
        double t0,t1,root;
        root = sqrt(discr);
        t0 = (- Bq - root) / (2*Aq);
        t1 = (- Bq + root) / (2*Aq);
        
        // make sure t0 is smaller
        if (t0 > t1) {
            double tmp = t0;
            t0 = t1;
            t1 = tmp;
        }
        
        if (t1 < 0) {
            result.hit = false;
            return result;
        }
        
        result.hit = true;

        if (t0 < 0) {
            result.t = t1;
        }else{
            result.t = t0;
        }
        
    }
       
    result.hitPoint = r.o() + r.d() * result.t;
    
    Vector3 h = result.hitPoint;
    Vector3 N;
    N.x = 2*A*h.x + D*h.y + E*h.z + G;
    N.y = 2*B*h.y + D*h.x + F*h.z + H;
    N.z = 2*C*h.z + E*h.x + F*h.y + I;
    N.normalize();
    result.normal = N;
    return result;
}