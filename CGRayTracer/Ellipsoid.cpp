//
//  Ellipsoid.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/22/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "Ellipsoid.h"

Ellipsoid::Ellipsoid(double a, double b, double c,Vector3 center):
Quadric()
{
    double a2,b2,c2;
    
    a2 = a*a;
    b2 = b*b;
    c2 = c*c;
    
    A = 1.0f/(a2);
    B = 1.0f/(b2);
    C = 1.0f/(c2);
    
    G = - 2 * center.x / (a2);
    H = - 2 * center.y / (b2);
    I = - 2 * center.z / (c2);
    
    J = -1 + (center.x * center.x) / a2 +
             (center.y * center.y) / b2 +
             (center.z * center.z) / c2;
}