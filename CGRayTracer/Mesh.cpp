//
//  Mesh.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 11/6/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "Mesh.h"

Mesh::Mesh(string filename, Vector3 position, double scale)
{
    vector<Face> faces;

    loadObj(filename,faces,position,scale);
    
    computeNormals(faces);
    
    cout << "Finished loading model: " << filename << "\n" <<
    "Vertices: " << vertices_.size() << " Triangles:" << triangles_.size() << "\n";
    
    buildOctree();
}

void Mesh::loadObj(string filename, vector<Face> & faces, Vector3 position,
                   double scale)
{
    read_obj(filename.c_str(), vertices_, faces);
    
    for (int i = 0; i < vertices_.size(); i++) {
        vertices_[i] *= scale;
        vertices_[i] += position;
    }
    
}

void Mesh::computeNormals(vector<Face> & faces)
{
    vector<vector<int> > vertexToFace(vertices_.size());
    vector<pair<double,Vector3> > faceInfo(faces.size());
    
    for (int i = 0; i < faces.size(); i++) {
        Face f = faces[i];
        
        for (int v = 0; v < f.size(); v++) {
            vertexToFace[f[v]].push_back(i);
        }
        
        Vector3 v1 = vertices_[f[0]] - vertices_[f[1]];
        Vector3 v2 = vertices_[f[2]] - vertices_[f[1]];
        
        Vector3 n = v1.cross(v2);
        faceInfo[i] = make_pair(n.length() / 2.0f, n.normalize());
    }
    
    normals_ = vector<Vector3>(vertices_.size());
    for (int i = 0; i < vertices_.size(); i++) {
        
        double areasum = 0;
        Vector3 normal(0,0,0);
        
        vector<int> * vtf = &(vertexToFace[i]);
        
        for (int j = 0; j < vtf->size(); j++) {
            double area = faceInfo[vtf->at(j)].first;
            areasum += area;
            normal += faceInfo[vtf->at(j)].second * area;
        }
        
        normals_[i] = -normal / areasum;
    }
    
    for (int i = 0; i < faces.size(); i++) {
        
        Face f = faces[i];
        
        Triangle t(f[0],f[1],f[2]);
        t.setPointers(&vertices_, &normals_, &texcoords_);
        t.setMaterial(material());
        
        triangles_.push_back(t);
    }
}

IntersectionPoint Mesh::intersect(Ray &r)
{
    return octree_->intersect(r);
    /*
    IntersectionPoint besthit;
    besthit.t = -1;
    besthit.hit = false;
    
    for (int i = 0; i < triangles_.size(); i++) {
        IntersectionPoint tmphit = triangles_[i].intersect(r);
        
        if (tmphit.hit && tmphit < besthit) {
            besthit = tmphit;
        }
    }
    
    return besthit;*/
}

void Mesh::setMaterial(Material *mat)
{
    SceneObject::setMaterial(mat);
    for (int i = 0; i < triangles_.size(); i++) {
        triangles_.at(i).setMaterial(mat);
    }
}

void Mesh::buildOctree()
{
    octree_ = new Octree();
    
    for (int i = 0; i < triangles_.size(); i++) {
        octree_->addObject(&triangles_[i]);
    }
    
    octree_->build();
    
}
