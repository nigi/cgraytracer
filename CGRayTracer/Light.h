//
//  Light.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 9/23/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__Light__
#define __CGRayTracer__Light__

#include <iostream>
#include <vector>
#include "LightIntensities.h"

using namespace std;

class Light {
    
    
public:
    virtual vector<LightIntensities> getIntensities(Vector3 worldpos){
        return vector<LightIntensities>(1);};
};

#endif /* defined(__CGRayTracer__Light__) */
