//
//  LightMaterial.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 11/8/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__LightMaterial__
#define __CGRayTracer__LightMaterial__

#include <iostream>
#include "Material.h"

class LightMaterial : public Material {
    
    Vector3 color_;
    
public:
    LightMaterial(Vector3 color):color_(color){};
    Vector3 shade(const IntersectionPoint & p, const Scene & scene, int recursionsToGo);

};

#endif /* defined(__CGRayTracer__LightMaterial__) */
