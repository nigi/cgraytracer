/*
 *  main.cpp
 *  raytracer
 *  Computer Graphics course exercise
 *
 *  Written by Changil Kim <kimc@inf.ethz.ch> and Alec Jacobson <jacobson@inf.ethz.ch> on 12/10/2011.
 *  Copyright 2011 ETH Zurich. All rights reserved.
 *
 *  Part of the code is based on the previous exercise code written by:
 *  Thomas Oskam, Michael Eigensatz, Hao Li, Balint Miklos, Raphael Hoever, Steven Poulakos, and Marcel Germann.
 */

// main function and GLUT windowing routine for the raytracer
//
// to compile: see COMPILE for detail
//
// Mac (GCC):      g++ -o raytracer main.cpp -framework OpenGL -framework GLUT
// Linux (GCC):    g++ -o raytracer main.cpp -lGL -lglut
// Windows (VC++): cl main.cpp /EHsc /link /OUT:raytracer.exe


#include <iostream>
#include <cstring>

#include <limits>

#include "exporter.h"
#include "utils.h"

#include "Scene.h"
#include "Sphere.h"
#include "PhongMaterial.h"
#include "PointLight.h"
#include "RayTracer.h"
#include "Module.h"

#if defined(__APPLE__) || defined(__MACH__)
// for Mac
#include <sys/time.h>
#include <GLUT/GLUT.h>

#elif defined(__linux__) || defined(UNIX)
// for Linux
#include <sys/time.h>
#include <GL/glut.h>

#elif defined(_WIN32) || defined(_WIN64)
// for Windows
#include <Windows.h>
#include <GL/glut.h>

#endif


// Globals

// size of the image to render
unsigned int g_width;
unsigned int g_height;

// image buffer
float* g_buffer;

// true iff the image buffer is ready
bool g_raytracing_complete;

// BMP image exporter
Exporter* g_exporter;

// constants for GLUT menu
enum Menu {
	MENU_START_RENDERER,
	MENU_EXPORT_IMAGE,
	MENU_EXIT,
	MENU_EMPTY
};

Module module_;

void initRendering(int argc, char *argv[])
{
	if(argc > 1){
		module_ = Module::createModule(std::string(argv[1]));
	}else
	{
		module_ = Module::createModule("A1");
		std::cout << "No arguments supplied! Starting Module A1!\n";
	}
}

// Functions - GLUT windowing

// init glut window
void initWindow(int argc, char *argv[]) {
    
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	
	glutInitWindowSize(g_width, g_height);
	glutInit(&argc, argv);
	glutCreateWindow("Ray Tracer");
	
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
}

// glut display callback function
void displayFunction(void) {
	glClear(GL_COLOR_BUFFER_BIT);

	if (g_raytracing_complete) {
		// Write rendered image into frame buffer
		glRasterPos2i(-1, -1);
		glDrawPixels(g_width, g_height, GL_RGB, GL_FLOAT, g_buffer);
	}

	glutSwapBuffers();
}

// glut reshape callback function
void reshapeFunction(int width, int height) {
	// reallocate the image buffer
	g_width = width;
	g_height = height;
    module_.scene_->camera()->setSize(width, height);
	delete [] g_buffer;
	g_buffer = new float[g_width * g_height * 3];

	g_raytracing_complete = false;
	
	glViewport(0, 0, g_width, g_height);
	
	std::cout << "Image buffer size: " << g_width << "x" << g_height << std::endl;
}


// for timing
unsigned long getTime(){
#if defined(_WIN32) || defined(_WIN64)
	LARGE_INTEGER frequency;
	LARGE_INTEGER tick;
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&tick);
	
	return (unsigned long)(tick.QuadPart * 1000 / frequency.QuadPart);
#else
	struct timeval tick;
	gettimeofday(&tick, NULL);
	
	return tick.tv_sec * 1000 + tick.tv_usec / 1000;
#endif
}

// function for rendering status updates
void update(int renderedPixels, int totalPixels) {
	if (renderedPixels % (totalPixels / 10) == 0) {
		std::cout << "[" << (renderedPixels / (totalPixels / 10)) * 10 << "%]" << std::flush;
		
		displayFunction();
	}
}



// Render the scene
void raytraceScene() {
	std::cout << "Rendering.... ";
	unsigned long startTime = getTime();

	memset(g_buffer, 0, sizeof(float) * g_width * g_height * 3);
	
    module_.render(g_buffer, g_width, g_height, &update);
        
	unsigned long endTime = getTime();
	std::cout << "[done]" << std::endl;
	std::cout << "Total rendering time: " << (endTime - startTime) / 1000. << " sec." << std::endl << std::endl;

	g_raytracing_complete = true;

	displayFunction();
}

// export the rendered image
void exportImage() {
	if (!g_exporter->exportImage(g_buffer, g_width, g_height)) {
		std::cerr << "Exporting to image failed!" << std::endl;
	} else {
		std::cout << "Image file exported." << std::endl;
	}
}

// release memory
void cleanup() {
	delete [] g_buffer;
	delete g_exporter;
}


// glut key callback function
void keyboardFunction(unsigned char key, int x, int y) {
	if (key == 27) {
		// ESC - exit
		cleanup();
		exit(0);
		
	} else if (key == 'r' || key == 'R') {
		// start raytracing
		raytraceScene();
		
	} else if (key == 'e' || key == 'E') {
		// export output
		exportImage();
	}
}


// glut menu callback function
void menuFunction(int value) {
	if (value == MENU_EXIT) {
		// exit
		cleanup();
		exit(0);
		
	} else if (value == MENU_START_RENDERER) {
		// start raytracing
		raytraceScene();
		
	} else if (value == MENU_EXPORT_IMAGE) {
		// export buffer to image
		exportImage();
	}
}


// raytracer main function
int main(int argc, char *argv[]) {
	
	// init image buffer
	g_raytracing_complete = false;
	g_width = 800;
	g_height = 600;
	g_buffer = new float[g_width * g_height * 3];
	memset(g_buffer, 0, sizeof(float) * g_width * g_height * 3);

    initRendering(argc,argv);

    
	// init image exporter
	g_exporter = new Exporter();

	// initialize glut window
	initWindow(argc, argv);

	// add glut callback functions
	glutDisplayFunc(displayFunction);
	glutReshapeFunc(reshapeFunction);
	glutKeyboardFunc(keyboardFunction);

	// add glut menu
	glutCreateMenu(menuFunction);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
	glutAddMenuEntry("Ray Tracer Menu", MENU_EMPTY);
	glutAddMenuEntry("----------------------", MENU_EMPTY);
	glutAddMenuEntry("Render Image", MENU_START_RENDERER);
	glutAddMenuEntry("Export image", MENU_EXPORT_IMAGE);
	glutAddMenuEntry("Exit", MENU_EXIT);

	// display status on console
	std::cout << std::endl
		<< "RayTracer ready!" << std::endl
		<< "-> Right click to open the menu." << std::endl
		<< "-> Press 'r' to start rendering, 'e' to export the image, and ESC to quit." << std::endl
		<< "-> Resize the window to change the size of the rendered image." << std::endl << std::endl;
	
	// enter glut event loop
	glutMainLoop();

	// cannot reach here

	return 0;
}
