//
//  StereoCamera.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/29/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__StereoCamera__
#define __CGRayTracer__StereoCamera__

#include <iostream>
#include "Camera.h"

class StereoCamera : public Camera {
    
    double eyeGap_;
    
    Vector3 eyePos1_;
    Vector3 eyePos2_;
    
    Vector3 eyeDiff1_;
    Vector3 eyeDiff2_;
    
public:
    StereoCamera():Camera(),eyeGap_(1.0f){
        update();
    };
    StereoCamera(Vector3 eyePos,
                 Vector3 eyeDir,
                 Vector3 eyeUp,
                 double d, double fovy, int pixelwidth, int pixelheight,
                 double eyeGap) : Camera(eyePos,eyeDir,eyeUp,d,fovy,pixelwidth,
                                         pixelheight),eyeGap_(eyeGap)
    {
        StereoCamera::update();
    };
    
    std::pair<Ray, Ray> generateRaysForPixel(double u, double v);
    std::pair<Ray, Ray> generateRaysForUV(double u, double v);
    
    void setCamera(Vector3 eyePosition, Vector3 eyeDirection,
                   Vector3 eyeUp, double d, double fovy);
    void setSize(int pixelwidth, int pixelheight);

private:
    void update();
    
};

#endif /* defined(__CGRayTracer__StereoCamera__) */
