//
//  EllipticCylinder.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/22/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "EllipticCylinder.h"

EllipticCylinder::EllipticCylinder(double a, double b, Vector3 center)
: Quadric()
{
    A = 1.0f / (a*a);
    C = 1.0f / (b*b);
    J = -1;
    
    
}