//
//  SphereSphereIntersection.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/22/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "SphereSphereIntersection.h"
#include <vector>
#include <algorithm>

IntersectionPoint SphereSphereIntersection::intersect(Ray &r)
{
    IntersectionPoint inter;
    
    double t0s0,t1s0, t0s1,t1s1;
    
    bool hit0 = sphere0_->intersections(r, t0s0, t1s0);
    bool hit1 = sphere1_->intersections(r, t0s1, t1s1);
    
    if (!(hit0 && hit1)) {
        inter.hit = false;
        return inter;
    }
    
    std::vector<pair<double, bool> > ts(4);
    ts[0] = make_pair(t0s0,false);
    ts[1] = make_pair(t1s0,false);
    ts[2] = make_pair(t0s1,true);
    ts[3] = make_pair(t1s1,true);
    
    std::sort(ts.begin(), ts.end());
    
    if (ts[2].first < 0) {
        inter.hit = false;
        return inter;
    }
    
    Sphere * s;
    if (ts[1].first < 0) {
        inter.t = ts[2].first;
        s = ts[2].second ? sphere1_ : sphere0_;
        
    }else{
        inter.t = ts[1].first;
        s = ts[1].second ? sphere1_ : sphere0_;
    }
    
    inter.hit = true;
    inter.hitPoint = r.o() + r.d() * inter.t;
    inter.normal = s->normalAtPoint(inter.hitPoint);
    inter.incident = r.d();
    inter.material = this->material();
    
    return inter;
}
