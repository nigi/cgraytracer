//
//  TexturePhongMaterial.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/29/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "TexturePhongMaterial.h"

TexturePhongMaterial::TexturePhongMaterial(std::string filename, double specExp)
:PhongMaterial(Vector3(0,0,0), Vector3(0,0,0), Vector3 (0,0,0),  specExp),
tex_(filename,TGATexture)
{
    tex_.setAntiAliasingRadius(10);
    bumpTex_ = NULL;
}

TexturePhongMaterial::TexturePhongMaterial(std::string filename,
                                           std::string bumpFilename,
                                           double specExp)
:PhongMaterial(Vector3(0,0,0), Vector3(0,0,0), Vector3 (0,0,0),  specExp),
tex_(filename,TGATexture)
{
    tex_.setAntiAliasingRadius(1);
    useBumpMap(bumpFilename);
}


Vector3 TexturePhongMaterial::shade(const IntersectionPoint &p, const Scene &scene, int recursionsToGo)
{
    IntersectionPoint ptmp = p;
    
    Vector3 texcol;
    if (bumpTex_ != NULL) {
        // use the normal map
        Vector3 values = (*bumpTex_)(p.texCoords.x,p.texCoords.y);
        Vector3 tbn;
        tbn.x = (2 * values.x - 1);
        tbn.y = (2 * values.y - 1);
        tbn.z = (2 * values.z - 1);
        
        ptmp.normal = p.tangent * tbn.x + p.cotangent * tbn.y + p.normal * tbn.z;
        ptmp.normal.normalize();
        
    }
    
    texcol = tex_(p.texCoords.x,p.texCoords.y);
    
    ambientColor_ = texcol;
    diffuseColor_ = texcol;
    specularColor_ = texcol;
    
    
    double c = cos(p.texCoords.x * 100) *
                cos(p.texCoords.y * 100);
    Vector3 texmark(c,c,c);
    
    //return ptmp.normal;
    return PhongMaterial::shade(ptmp, scene, recursionsToGo);
}

void TexturePhongMaterial::useBumpMap(std::string filename)
{
    bumpTex_ = new Texture(filename,TGATexture);
    
    if (bumpTex_ != NULL) {
        bumpTex_->setAntiAliasingRadius(1);
    }
}
