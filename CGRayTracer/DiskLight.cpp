//
//  DiskLight.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 11/8/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include <cstdlib>
#include "DiskLight.h"

DiskLight::DiskLight(Vector3 center,Vector3 orientation, double radius,
                     Vector3 color, double ambient, double diffuse, double specular)
: center_(center),
orientation_(orientation),
radius_(radius),
samples_(50),

color_(color),
ambientI(ambient),
diffuseI(diffuse),
specularI(specular)
{
    orientation_.normalize();
    Vector3 t(rand(),-orientation.x,-orientation.z);
    v1_ = t.cross(orientation).normalize();
    v2_ = v1_.cross(orientation).normalize();
    
    lmat_ = new LightMaterial(color_);
}

vector<LightIntensities> DiskLight::getIntensities(Vector3 worldpos)
{
    vector<LightIntensities> result;
    for (int i = 0; i < samples_; i++) {
        LightIntensities l;
        double d1 = radius_ * (((double)rand() / RAND_MAX)*2.0f - 1.0f);
        double d2 = radius_ * (((double)rand() / RAND_MAX)*2.0f - 1.0f);
        l.hitPoint = v1_ * d1 + v2_ * d2 + center_;
                
        if ((l.hitPoint - center_).lengthSquared() > radius_*radius_ ) {
            i--;
            continue;
        }
        
        
        l.ambientColor = color_ * ambientI;
        l.diffuseColor = color_ * diffuseI;
        l.specularColor = color_ * specularI;
                
        l.light = this;
        result.push_back(l);
    }
    return result;
}

void DiskLight::setNumberOfSamples(int s)
{
    samples_ = s;
}

IntersectionPoint DiskLight::intersect(Ray &r)
{
    IntersectionPoint result;
    result.hit = false;
    // First do ray plane intersection
    
    double t = (orientation_.dot(r.o() - center_) / (orientation_.dot(r.d())));
    Vector3 P = r.o() + r.d()*t;
    
    if ((P-center_).lengthSquared() > radius_*radius_) return result;
    
    result.t = t;
    result.hitPoint = P;
    result.normal = orientation_;
    result.material = lmat_;
    result.hit = true;
    
    return result;
}
