//
//  Material.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 9/23/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__Material__
#define __CGRayTracer__Material__

#include <iostream>
#include "utils.h"
#include "IntersectionPoint.h"

using namespace std;

class Scene;

class Material {

public:
    
    virtual Vector3 shade(const IntersectionPoint & p, const Scene & scene, int recursionsToGo) = 0;
        
};

#endif /* defined(__CGRayTracer__Material__) */
