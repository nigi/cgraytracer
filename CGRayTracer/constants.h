//
//  Constants.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/12/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef CGRayTracer_constants_h
#define CGRayTracer_constants_h

#define INTERSECTION_OFFSET 0.0000001

#define MAX_RECURSION_DEPTH 10

#endif
