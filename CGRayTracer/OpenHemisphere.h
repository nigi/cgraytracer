//
//  OpenHemisphere.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/22/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__OpenHemisphere__
#define __CGRayTracer__OpenHemisphere__

#include <iostream>
#include "SceneObject.h"

class OpenHemisphere : public SceneObject {
    
    Vector3 center_;
    double radius_;
    
    Material * interiorMaterial_;
    
public:
    OpenHemisphere(Vector3 c,double r):center_(c),radius_(r){};
    IntersectionPoint intersect(Ray &r);
void setInteriorMaterial(Material * mat){ interiorMaterial_ = mat;};
};

#endif /* defined(__CGRayTracer__OpenHemisphere__) */
