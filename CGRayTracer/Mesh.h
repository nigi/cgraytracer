//
//  Mesh.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 11/6/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__Mesh__
#define __CGRayTracer__Mesh__

#include <iostream>
#include <vector>
#include "SceneObject.h"
#include "read_obj.h"
#include "Triangle.h"
#include "Octree.h"

using namespace std;

class Mesh : public SceneObject {
    
    vector<Triangle> triangles_;
    
    vector<Vector3> vertices_;
    vector<Vector3> normals_;
    vector<Vector2> texcoords_;
    
    Octree * octree_;
    
public:
    Mesh(string filename, Vector3 position, double scale);
    IntersectionPoint intersect(Ray &r);
    void setMaterial(Material * mat);
    
private:
    void loadObj(string filename, vector<Face> & faces, Vector3 position,
                 double scale);
    void computeNormals(vector<Face> & faces);
    void buildOctree();
};

#endif /* defined(__CGRayTracer__Mesh__) */
