//
//  EllipticCylinder.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/22/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__EllipticCylinder__
#define __CGRayTracer__EllipticCylinder__

#include <iostream>
#include "Quadric.h"

class EllipticCylinder : public Quadric {
    
public:
    EllipticCylinder(double a, double b,Vector3 center);
};

#endif /* defined(__CGRayTracer__EllipticCylinder__) */
