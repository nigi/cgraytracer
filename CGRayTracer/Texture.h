//
//  Texture.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/29/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__Texture__
#define __CGRayTracer__Texture__

#include <iostream>
#include "utils.h"
#include "read_tga.h"

enum TextureType {
    TGATexture
    };

class Texture {
    
    Image img_;
    int antiAliasRadius_;
    
public:
    Texture(std::string filename, TextureType type);
    
    Vector3 operator()(double u, double v);
    Vector3 operator()(int x, int y);
    void setAntiAliasingRadius(int r);

    
private:
    void loadFromTGA(std::string filename);
    
};

#endif /* defined(__CGRayTracer__Texture__) */
