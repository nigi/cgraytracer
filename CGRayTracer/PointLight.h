//
//  PointLight.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 9/23/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__PointLight__
#define __CGRayTracer__PointLight__

#include <iostream>
#include "Light.h"
#include "utils.h"

class PointLight : public Light {
    
    Vector3 center_;
    Vector3 color_;
    
    double ambientI;
    double diffuseI;
    double specularI;
    
public:
    PointLight(Vector3 center, Vector3 color, double ambient, double diffuse, double specular):center_(center),color_(color), ambientI(ambient), diffuseI(diffuse), specularI(specular) {};
    vector<LightIntensities> getIntensities(Vector3 worldpos);
};

#endif /* defined(__CGRayTracer__PointLight__) */
