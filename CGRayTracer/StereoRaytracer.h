//
//  StereoRaytracer.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/29/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__StereoRaytracer__
#define __CGRayTracer__StereoRaytracer__

#include <iostream>
#include "RayTracer.h"
#include "StereoCamera.h"

class StereoRaytracer : public RayTracer {
    
    StereoCamera * cam_;
    
public:
    StereoRaytracer(StereoCamera * cam):RayTracer(),cam_(cam){};
    void traceScene(Scene& scene, float * g_buffer,
                    unsigned int g_width, unsigned int g_height,
                    void (*update)(int,int));
};

#endif /* defined(__CGRayTracer__StereoRaytracer__) */
