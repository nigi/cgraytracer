//
//  Ellipsoid.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/22/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__Ellipsoid__
#define __CGRayTracer__Ellipsoid__

#include <iostream>
#include "Quadric.h"

class Ellipsoid : public Quadric {
    
    
public:
    Ellipsoid(double a, double b, double c,Vector3 center);
};


#endif /* defined(__CGRayTracer__Ellipsoid__) */
