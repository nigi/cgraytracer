//
//  Texture.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/29/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "Texture.h"

Texture::Texture(std::string filename, TextureType type)
{
    antiAliasRadius_ = 0;
    
    switch (type) {
        case TGATexture:
            loadFromTGA(filename);
            break;
            
        default:
            break;
    }
}


void Texture::loadFromTGA(std::string filename)
{
    Image * imp = read_tga(filename.c_str());
    
    if (imp == NULL) {
		std::cerr << "failed to load " << filename << std::endl;
	}else{
        img_ = *imp;
        std::cout << "loaded image: " << filename << "\n";
        std::cout << img_.width << " " << img_.height << "\n";
    }
}

Vector3 Texture::operator()(double u, double v)
{
    u = u * img_.width;
    v = v * img_.height;
    
    int x = (int)floor(u);
    int y = (int)floor(v);
    
    double xratio = u - x;
    double yratio = v - y;
    
    double negxratio = 1.0f - xratio;
    double negyratio = 1.0f - yratio;
    
    // average over some pixels in window
    
//    int r = antiAliasRadius_;
//    int i = 0;
//    Vector3 color(0,0,0);
//    
//    for (int xi = x - r;  xi <= x + r; xi++) {
//        for (int yi = y - r; yi <= y + r; yi++) {
//            if (xi < 0 || xi >= img_.width ||
//                yi < 0 || yi >= img_.height) {
//                continue;
//            }
//            
//            color += ((*this)(xi,yi));
//            i++;
//        }
//    }
    
    // perform bilinear filtering
    // get the four colours and weight them
    Vector3 CXY = (*this)(x,y);
    Vector3 CX1Y = (*this)(x+1,y);
    Vector3 CXY1 = (*this)(x,y+1);
    Vector3 CX1Y1 = (*this)(x+1,y+1);
    
    
    Vector3 colorX1 = CXY * negxratio + CX1Y * xratio;
    Vector3 colorX2 = CXY1 * negxratio + CX1Y1 * xratio;
    Vector3 color = colorX1 * negyratio + colorX2 * yratio;

    return color;
}

Vector3 Texture::operator()(int x, int y)
{
    x = x < 0 ? 0 : (x >= img_.width ? img_.width-1 : x);
    y = y < 0 ? 0 : (y >= img_.height ? img_.height-1 : y); 
    
    
    unsigned int pos = 3 * (y * img_.width + x);
    
    // read color values
    unsigned char r = img_.pixels[pos + 2];
    unsigned char g = img_.pixels[pos + 1];
    unsigned char b = img_.pixels[pos];
    
    Vector3 result;
    result.x = ((int) r) / 255.0f;
    result.y = ((int) g) / 255.0f;
    result.z = ((int) b) / 255.0f;
    
    return result;
}

void Texture::setAntiAliasingRadius(int r)
{
    antiAliasRadius_ = r;
}
