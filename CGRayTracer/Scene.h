//
//  Scene.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 9/23/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__Scene__
#define __CGRayTracer__Scene__

#include <iostream>
#include <vector>
#include "SceneObject.h"
#include "Light.h"
#include "Camera.h"

using namespace std;

class Scene {
    
    vector<SceneObject*> objects_;
    vector<Light*> lights_;
    Camera * camera_;
    
public:

    Scene(Camera * cam):camera_(cam){};
    Vector3 ambientColor;
    
    void addObject(SceneObject* object);
    void addLight(Light* light);
    
    void setCamera(Camera * cam){
        camera_ = cam;
    };
    
    const vector<SceneObject*>& objects() const;
    const vector<Light*>& lights() const;
    Camera * camera();
    
};

#endif /* defined(__CGRayTracer__Scene__) */
