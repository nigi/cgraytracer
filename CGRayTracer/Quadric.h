//
//  Quadric.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/22/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__Quadric__
#define __CGRayTracer__Quadric__

#include <iostream>

#include "SceneObject.h"

class Quadric : public SceneObject {
    
protected:
    // The quadric parameters
    double A,B,C,D,E,F,G,H,I,J;
    Vector3 center;
    Quadric():A(0),B(0),C(0),D(0),E(0),F(0),G(0),H(0),I(0),J(0){};
    Quadric(double a,
            double b,
            double c,
            double d,
            double e,
            double f,
            double g,
            double h,
            double i,
            double j):A(a),B(b),C(c),D(d),E(e),F(f),G(g),
                     H(h),I(i),J(j){};
public:
    virtual IntersectionPoint intersect(Ray &r);
};

#endif /* defined(__CGRayTracer__Quadric__) */
