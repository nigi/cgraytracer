//
//  ReflectivePhongMaterial.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/13/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "ReflectivePhongMaterial.h"
#include "RayTracer.h"

ReflectivePhongMaterial::ReflectivePhongMaterial(Vector3 ambientC,
                                                 Vector3 diffuseC,
                                                 Vector3 specularC,
                                                 double specularExp):
PhongMaterial(ambientC,diffuseC,specularC,specularExp)
{
    
}

Vector3 ReflectivePhongMaterial::shade(const IntersectionPoint & p,
              const Scene & scene,int recursionsToGo)
{
    Vector3 phongColor = PhongMaterial::shade(p, scene, recursionsToGo);
    Vector3 reflectionDir = p.incident + p.normal * (-p.incident).dot(p.normal) * 2;
    Ray r = Ray(p.hitPoint, reflectionDir);
    Vector3 reflectionColor = RayTracer::shadeRay(r, scene, recursionsToGo);
    
    return phongColor * 0.5f + reflectionColor * 0.5;
}
