//
//  TexturePhongMaterial.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/29/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__TexturePhongMaterial__
#define __CGRayTracer__TexturePhongMaterial__

#include <iostream>
#include "PhongMaterial.h"
#include "Texture.h"

class TexturePhongMaterial : public PhongMaterial {
    
    Texture tex_;
    Texture * bumpTex_;

    
public:
    TexturePhongMaterial(std::string filename, double specExp);
    TexturePhongMaterial(std::string filename, std::string bumpFilename,double specExp);
    void useBumpMap(std::string filename);
    
    Vector3 shade(const IntersectionPoint & p, const Scene & scene,int recursionsToGo);
};

#endif /* defined(__CGRayTracer__TexturePhongMaterial__) */
