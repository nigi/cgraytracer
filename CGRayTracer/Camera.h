//
//  Camera.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 9/23/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__Camera__
#define __CGRayTracer__Camera__

#include <iostream>
#include "utils.h"
#include "Ray.h"

class Camera{
protected:
    
    Vector3 eyePos_;
    Vector3 eyeDir_;
    Vector3 eyeUp_;
    
    Vector3 right_;
    
    double d_;      // distance from eyePos to center of projection plane
    double fovy_;   // angle between top-right, eyePos and bottom-right corner
                    // in rad
    
    double tanFovy_;
    double tanFovx_;
    
    double h_; // world-space height of projection plane
    double w_; // world-space width of projection plane
    
    int width_;
    int height_;
    
public:
    
    Camera();
    Camera(Vector3 eyePosition, Vector3 eyeDirection,
           Vector3 eyeUp, double d, double fovy, int pixelwidth, int pixelheight);
    
    Ray generateRayForPixel(double u, double v);
    Ray generateRayForUV(double u, double v);
    void moveTo(Vector3 eyePos);
    virtual void setCamera(Vector3 eyePosition, Vector3 eyeDirection,
                      Vector3 eyeUp, double d, double fovy);
    virtual void setSize(int pixelwidth, int pixelheight);
    
protected:
    void update();
    
};


#endif /* defined(__CGRayTracer__Camera__) */
