//
//  Module.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/12/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__Module__
#define __CGRayTracer__Module__

#include <iostream>
#include "Scene.h"
#include "RayTracer.h"

enum ModuleName {
    A1,
    B1,
    B2,
    B3,
    B4
    };

class Module {
    
private:
    RayTracer * tracer_;
    
public:
    Scene * scene_;

    std::string name;
    
public:
    Module();
    
    void render(float* g_buffer, int g_width, int g_height, void (*update)(int,int));
    static Module createModule(std::string name);
    
};

#endif /* defined(__CGRayTracer__Module__) */
