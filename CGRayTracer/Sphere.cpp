//
//  Sphere.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 9/23/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "Sphere.h"

IntersectionPoint Sphere::intersect(Ray &r)
{
    
    double t0,t1;
    bool intersection = intersections(r, t0, t1);
  
    IntersectionPoint result = IntersectionPoint();

    if (!intersection) {
        result.hit = false;
        return result;
    }
    
    if (t0 < 0)
    {
        result.t = t1;
    }else{
        result.t = t0;
    }
    
    result.hit = true;
    result.incident = r.d();
    result.object = this;
    result.hitPoint = r.o() + r.d() * result.t;
    result.normal = (result.hitPoint - center_).normalize();
    result.material = this->material();

    
    // Project point into plane
    Vector3 Q = result.normal - northpole_ * result.normal.dot(northpole_) /
                                        northpole_.dot(northpole_);
    Q.normalize();
    
    result.texCoords.x = atan2(Q.dot(-seconddir_),
                               Q.dot(primedir_)) / (2*M_PI) + .5f;

    // map angle of y to v
    result.texCoords.y = asin(-result.normal.dot(northpole_)) / (M_PI) + 0.5f;
    
    // map y directly to v
    //result.texCoords.y = -result.normal.dot(northpole)/2.0f + 0.5f;
    
    // Compute tangent and cotangent
    result.cotangent = -northpole_.cross(result.normal).normalize();
    result.tangent = -result.normal.cross(result.cotangent).normalize();
    
    return result;
}

// Returns true if there is an intersection, false otherwise
// Stores the smaller value of t in t0 the larger in t1
bool Sphere::intersections(Ray &r, double& t0, double& t1)
{
    Vector3 ot = r.o() - center_;
    double A = r.d().dot(r.d());
    double B = 2 * (r.d().dot(ot));
    double C = ot.dot(ot) - radius_*radius_;
        
    double discr = B*B - 4*A*C;
    
    if(discr < 0)
    {
        return false;
    }
    
    double q;
    
    if(B < 0)
    {
        q = (-B - sqrt(discr)) / 2.0f;
    }else{
        q = (-B + sqrt(discr)) / 2.0f;
    }
    
    t0 = q / A;
    t1 = C / q;
    
    if (t0 > t1) {
        double tmp = t0;
        t0 = t1;
        t1 = tmp;
    }
    
    if (t1 < 0)
    {
        return false;
    }else{
        return true;
    }
}

bool Sphere::insideSphere(Vector3 p)
{
    return (p - center_).lengthSquared() < (radius_ * radius_);
}

Vector3 Sphere::normalAtPoint(Vector3 p)
{
    return (p - center_).normalize();
}

void Sphere::setNorthpole(Vector3 north, Vector3 prime)
{
    northpole_ = north;
    northpole_.normalize();
    seconddir_ = -northpole_.cross(prime).normalize();
    primedir_ = northpole_.cross(seconddir_).normalize();
    
    
}