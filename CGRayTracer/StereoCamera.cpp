//
//  StereoCamera.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/29/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "StereoCamera.h"

std::pair<Ray,Ray> StereoCamera::generateRaysForPixel(double x, double y)
{
    double u,v;
    u = (x + 0.5f) / width_ ;
    v = (y + 0.5f) / height_;
    return generateRaysForUV(u, v);
}

std::pair<Ray, Ray> StereoCamera::generateRaysForUV(double u, double v)
{
    // Compute right vector in projection plane
    double x = (u - 0.5f) * w_;
    double y = (v - 0.5f) * h_;
    
    Vector3 rayDir = eyeDir_ * d_ + eyeUp_ * y + right_ * x;
    
    Vector3 rayDir1 = rayDir - eyeDiff1_;
    Vector3 rayDir2 = rayDir - eyeDiff2_;
    
    rayDir1.normalize();
    rayDir2.normalize();
    
    
    return std::make_pair(Ray(eyePos1_,rayDir1), Ray(eyePos2_,rayDir2));
}

void StereoCamera::update()
{
    std::cout << "update stereo\n";
    Camera::update();
    right_.normalize();
    eyePos1_ = eyePos_ + right_ * 0.5 * eyeGap_;
    eyePos2_ = eyePos_ - right_ * 0.5 * eyeGap_;
    
    eyeDiff1_ = eyePos1_ - eyePos_;
    eyeDiff2_ = eyePos2_ - eyePos_;
}

void StereoCamera::setCamera(Vector3 eyePosition, Vector3 eyeDirection,
                             Vector3 eyeUp, double d, double fovy)
{
    Camera::setCamera(eyePosition, eyeDirection, eyeUp, d, fovy);
    StereoCamera::update();
}

void StereoCamera::setSize(int pixelwidth, int pixelheight)
{
    std::cout << "update stereo\n";

    Camera::setSize(pixelwidth, pixelheight);
    StereoCamera::update();
}