//
//  Octree.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 11/8/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "Octree.h"
#include <limits>
#include <cstring>

Octree::Octree() :
isLeaf_(true)
{
}

IntersectionPoint Octree::intersect(Ray &r)
{
    IntersectionPoint result;
    result.hit = false;
    
    
    // Check if ray intersects with bounds
    Vector3 o = r.o();
    Vector3 invd = Vector3(1.0f,1.0f,1.0f) / r.d();
    
    Vector3 min = (bounds_.min - o) * invd;
    Vector3 max = (bounds_.max - o) * invd;
    
    double tmp;
    if (min.x > max.x) {
        tmp = min.x;
        min.x = max.x;
        max.x = tmp;
    }
    
    if (min.y > max.y) {
        tmp = min.y;
        min.y = max.y;
        max.y = tmp;
    }
    
    if (min.z > max.z) {
        tmp = min.z;
        min.z = max.z;
        max.z = tmp;
    }
    
    
    double tmin = std::max(min.x,std::max(min.y,min.z));
    double tmax = std::min(max.x,std::min(max.y,max.z));
    
    tmin = tmin < 0 ? 0 : tmin;
    tmax = tmax < 0 ? 0 : tmax;
    
    if (tmin >= tmax) return result; // No Hit!!
    
    /*
    float tmin, tmax, tymin, tymax, tzmin, tzmax;
    if (r.d().x >= 0) {
        tmin = (bounds_.min.x - r.o().x) / r.d().x;
        tmax = (bounds_.max.x - r.o().x) / r.d().x;
    }
    else {
        tmin = (bounds_.max.x - r.o().x) / r.d().x;
        tmax = (bounds_.min.x - r.o().x) / r.d().x;
    }
    if (r.d().y >= 0) {
        tymin = (bounds_.min.y- r.o().y) / r.d().y;
        tymax = (bounds_.max.y - r.o().y) / r.d().y;
    }
    else {
        tymin = (bounds_.max.y - r.o().y) / r.d().y;
        tymax = (bounds_.min.y - r.o().y) / r.d().y;
    }
    if ( (tmin > tymax) || (tymin > tmax) )
        return result;
    if (tymin > tmin)
        tmin = tymin;
    if (tymax < tmax)
        tmax = tymax;
    if (r.d().z >= 0) {
        tzmin = (bounds_.min.z - r.o().z) / r.d().z;
        tzmax = (bounds_.max.z - r.o().z) / r.d().z;
    }
    else {
        tzmin = (bounds_.max.z - r.o().z) / r.d().z;
        tzmax = (bounds_.min.z - r.o().z) / r.d().z;
    }
    if ( (tmin > tzmax) || (tzmin > tmax) )
        return result;
    if (tzmin > tmin)
        tmin = tzmin;
    if (tzmax < tmax)
        tmax = tzmax;
    if (!((tmax > 0) )) return result;
    */
    if (isLeaf_) {
        // Go through all objects and return nearest intersection
        IntersectionPoint inter;
        inter.hit = false;
        double mint = (std::numeric_limits< double >::max)();
        
        for(int i = 0; i < objects_.size(); i++)
        {
            IntersectionPoint tinter = objects_[i]->intersect(r);
            
            if (tinter.hit && tinter.t < mint && tinter.t > 0)
            {
                mint = tinter.t;
                inter = tinter;
            }
        }
        return inter;
    }
    
    // Go through all objects and return nearest intersection
    IntersectionPoint inter;
    inter.hit = false;
    double mint = (std::numeric_limits< double >::max)();
    
    for (int i = 0; i < 8; i++) {
        IntersectionPoint tinter = children_[i]->intersect(r);
        
        if (tinter.hit && tinter.t < mint && tinter.t > 0)
        {
            mint = tinter.t;
            inter = tinter;
        }
    }
    
    return inter;
}

void Octree::addObject(SceneObject *object)
{
    objects_.push_back(object);
}

void Octree::build()
{
    if (objects_.size() <= OCTREE_LEAVE_THRESH){
        isLeaf_ = true;
        return; // This is a leaf
    }
    
    isLeaf_ = false;
    computeBoundingBox();
    buildIn(bounds_, OCTREE_MAX_DEPTH);
}

void Octree::buildIn(AABoundingBox bounds, size_t depthToGo)
{
    bounds_ = bounds;
    if (objects_.size() <= OCTREE_LEAVE_THRESH || depthToGo == 0){
        isLeaf_ = true;
        return; // This is a leaf
    }
    
    std::cout << "depth: " << depthToGo << "\n";
    
    depthToGo--;
    isLeaf_ = false;

    children_.clear();
    for (int i = 0; i < 8; i++) {
        children_.push_back(new Octree());
    }
    
    bool octants[8] = {false};
    for (int i = 0; i < objects_.size(); i++) {
        memset(octants, false, sizeof(bool)*8);
        
        AABoundingBox b = objects_[i]->boundingBox();
        
        for (int o = 0; o < 8; o++) {
            int code = octantCode(bounds_.center, b.corners[o]);
            if (!octants[code]) children_[code]->addObject(objects_[i]);
            octants[code] = true; // so this object is not inserted again
        }
    }
    
    // Build all childnodes with the inserted objects
    for (int i = 0; i < 8; i++) {
        AABoundingBox b(bounds_.center, bounds_.corners[i]);
        // ATTENTION: the above used corners[i] has to be in the same octant
        //            as given by the octant code i
        children_[i]->buildIn(b, depthToGo);
    }
    
    objects_.clear();
}

int Octree::octantCode(Vector3 &center, Vector3 &point)
{
    int code = 0;
    if (point.x > center.x)
        code = code | 1;
    if (point.y > center.y)
        code = code | 2;
    if (point.z > center.z)
        code = code | 4;
    return code;
}

void Octree::computeBoundingBox()
{
    if (objects_.size() < 1) {
        isLeaf_ = true;
        return;
    }
    
    // Start with the first BB and extend it with all the others
    AABoundingBox bounds = objects_[0]->boundingBox();
    for (int i = 1; i < objects_.size(); i++) {
        AABoundingBox box = objects_[i]->boundingBox();
        bounds.extend(box);
    }
    
    bounds_ = bounds;
}

AABoundingBox Octree::boundingBox()
{
    return bounds_;
}
