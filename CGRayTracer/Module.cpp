//
//  Module.cpp
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 10/12/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#include "Module.h"
#include "Sphere.h"
#include "Ellipsoid.h"
#include "EllipticCylinder.h"
#include "OpenHemisphere.h"
#include "SphereSphereIntersection.h"
#include "PhongMaterial.h"
#include "ReflectivePhongMaterial.h"
#include "RefractivePhongMaterial.h"

#include "PointLight.h"

// Exercise 3
#include "StereoCamera.h"
#include "StereoRaytracer.h"
#include "TexturePhongMaterial.h"
#include "Mesh.h"
#include "Octree.h"
#include "DiskLight.h"

#include <string>

Module::Module()
{
    
}

void Module::render(float *g_buffer, int g_width, int g_height,
                    void (*update)(int, int))
{
    std::cout << "Render Module " << name << "\n";
    tracer_->traceScene(*scene_, g_buffer, g_width, g_height, update);
}

Module Module::createModule(std::string name)
{
    std::cout << "Creating module with name: " << name << "\n";
    
    Module m = Module();
    m.name = name;
    Camera * cam = new Camera();
    m.scene_ = new Scene(cam);
    m.tracer_ = new RayTracer();
    
    Sphere * s = new Sphere(Vector3(0,0,0), 2.0f);
    Sphere * s1 = new Sphere(Vector3(1.25,1.25,3), .5f);
    Light * l1 = new PointLight(Vector3(10,10,10), Vector3(1,1,1),
                                0.0f,1.0f,1.0f);
    
    if(name == "A1")
    {
        // Simple scene with spheres and standard shading
        s->setMaterial(new PhongMaterial(Vector3(0.75,0,0),
                                         Vector3(1,0,0),
                                         Vector3(1,1,1),
                                         32.0f));
        s1->setMaterial(new PhongMaterial(Vector3(0,0,0.75),
                                          Vector3(0,0,1),
                                          Vector3(0.5,0.5,1),
                                          16.0f));
        
        m.scene_->addObject(s);
        m.scene_->addObject(s1);
        m.scene_->addLight(l1);
        m.scene_->ambientColor = Vector3(1,1,1) * 0.2f;

    }else if(name == "B1")
    {
        // Recursive raytracing with reflection and refraction
        // Simple scene with spheres and standard shading
        s->setMaterial(new ReflectivePhongMaterial(Vector3(0.75,0,0),
                                         Vector3(1,0,0),
                                         Vector3(1,1,1),
                                         32.0f));
        s1->setMaterial(new RefractivePhongMaterial(Vector3(0,0,0.75),
                                          Vector3(0,0,1),
                                          Vector3(0.5,0.5,1),
                                          16.0f,1.5f));
        
        m.scene_->addObject(s);
        m.scene_->addObject(s1);
        m.scene_->addLight(l1);
        m.scene_->ambientColor = Vector3(1,1,1) * 0.2f;
        
    }else if(name == "B2")
    {
        // Standard scene + antialiasing
        s->setMaterial(new PhongMaterial(Vector3(0.75,0,0),
                                         Vector3(1,0,0),
                                         Vector3(1,1,1),
                                         32.0f));
        s1->setMaterial(new PhongMaterial(Vector3(0,0,0.75),
                                          Vector3(0,0,1),
                                          Vector3(0.5,0.5,1),
                                          16.0f));
        
        m.scene_->addObject(s);
        m.scene_->addObject(s1);
        m.scene_->addLight(l1);
        m.scene_->ambientColor = Vector3(1,1,1) * 0.2f;
        
        // Antialiasing
        m.tracer_->setRayNumber(16);
        
    }else if(name == "B3")
    {
		// Scene with quadrics
        Ellipsoid * e = new Ellipsoid(0.25, 0.75, 0.5, Vector3(1.25,1.25,3));
        e->setMaterial(new PhongMaterial(Vector3(0,0,0.75),
                                         Vector3(0,0,1),
                                         Vector3(0.5,0.5,1),
                                         16.0f));
        EllipticCylinder * ec = new EllipticCylinder(2,1,Vector3(0,0,0));
        ec->setMaterial(new PhongMaterial(Vector3(0.75,0,0),
                                          Vector3(1,0,0),
                                          Vector3(1,1,1),
                                          32.0f));
        m.scene_->addObject(ec);
        m.scene_->addObject(e);
        m.scene_->addLight(l1);
        m.scene_->ambientColor = Vector3(1,1,1) * 0.2f;

        
    }else if(name == "B4")
    {
		// Scene with Boolean Operations
        OpenHemisphere * o = new OpenHemisphere(Vector3(0,0,0),2);
        o->setMaterial(new PhongMaterial(Vector3(0.75,0,0),
                                         Vector3(1,0,0),
                                         Vector3(1,1,1),
                                         32.0f));
        o->setInteriorMaterial(new PhongMaterial(Vector3(0.75,0.75,0),
                                                 Vector3(1,1,0),
                                                 Vector3(1,1,1),
                                                 32.0f));
        
        SphereSphereIntersection * ss = new SphereSphereIntersection(
                            new Sphere(Vector3(1.25,1.25f,3.0f),.5f),
                            new Sphere(Vector3(0.25,1.25f,3.0f),1));
        
        ss->setMaterial(new PhongMaterial(Vector3(0,0,0.75),
                                          Vector3(0,0,1),
                                          Vector3(0.5,0.5,1),
                                          16.0f));

        m.scene_->addObject(ss);
        m.scene_->addObject(o);
        m.scene_->addLight(l1);
        m.scene_->ambientColor = Vector3(1,1,1) * 0.2f;

	}else if(name == "extra"){
		OpenHemisphere * o = new OpenHemisphere(Vector3(0,0,0),2);
        o->setMaterial(new ReflectivePhongMaterial(Vector3(0.75,0,0),
                                         Vector3(1,0,0),
                                         Vector3(1,1,1),
                                         32.0f));
        o->setInteriorMaterial(new ReflectivePhongMaterial(Vector3(0.75,0.75,0),
                                                 Vector3(1,1,0),
                                                 Vector3(1,1,1),
                                                 32.0f));
        
        SphereSphereIntersection * ss = new SphereSphereIntersection(
                            new Sphere(Vector3(1.25,1.25f,3.0f),.5f),
                            new Sphere(Vector3(0.25,1.25f,3.0f),1));
        
        ss->setMaterial(new PhongMaterial(Vector3(0,0,0.75),
                                          Vector3(0,0,1),
                                          Vector3(0.5,0.5,1),
                                          16.0f));

		Sphere * sground = new Sphere(Vector3(0,-10000,0),9998);
        sground->setMaterial(new ReflectivePhongMaterial(Vector3(0,0,0.5),Vector3(1,1,1),Vector3(1,1,1),32.0f));
        m.scene_->addObject(sground);
        
        m.scene_->addObject(ss);
        m.scene_->addObject(o);
        m.scene_->addLight(l1);
        m.scene_->ambientColor = Vector3(1,1,1) * 0.2f;

    }else if (name == "C1")
    {
        // Stereoscopic rendering
        StereoCamera * cam = new StereoCamera(Vector3(0,0,10),
                                              Vector3(0,0,-1),
                                              Vector3(0,1,0),
                                              8.5f,
                                              40.0f / 180.0f * M_PI,
                                              800,
                                              600,
                                              1);
        m.scene_->setCamera(cam);
        StereoRaytracer * r = new StereoRaytracer(cam);
        m.tracer_ = r;
        
        s->setMaterial(new TexturePhongMaterial("texture/Earth.tga",
                                                "texture/EarthNormal.tga",
                                                32.0f));
        s1->setMaterial(new TexturePhongMaterial("texture/Moon.tga",
                                                 "texture/MoonNormal.tga",
                                                 16.0f));
        
        s->setNorthpole(Vector3(0,1,1), Vector3(-1,1,-1));
        s1->setNorthpole(Vector3(0,1,1), Vector3(-1,1,-1));
        m.scene_->addObject(s);
        m.scene_->addObject(s1);
        m.scene_->addLight(l1);
        m.scene_->ambientColor = Vector3(1,1,1) * 0.2f;
        
    }else if (name == "C2")
    {
        // Texture mapping
        
        s->setNorthpole(Vector3(0,1,1), Vector3(-1,1,-1));
        s1->setNorthpole(Vector3(0,1,1), Vector3(-1,1,-1));
        s->setMaterial(new TexturePhongMaterial("texture/Earth.tga",
                                                "texture/EarthNormal.tga",
                                                32.0f));
        s1->setMaterial(new TexturePhongMaterial("texture/Moon.tga",
                                                 "texture/MoonNormal.tga",
                                                 16.0f));
        
        m.scene_->addObject(s);
        m.scene_->addObject(s1);
        m.scene_->addLight(l1);
        m.scene_->ambientColor = Vector3(1,1,1) * 0.2f;
        
        
    }else if (name == "C3")
    {
        // Mesh loading
        Mesh * mesh = new Mesh("mesh/sphere.obj",Vector3(0,0,0),2.0f);
        Mesh * mesh1 = new Mesh("mesh/sphere.obj",Vector3(1.25,1.25,3), .5f);
        
        mesh->setMaterial(new PhongMaterial(Vector3(0.75,0,0),
                                            Vector3(1,0,0),
                                            Vector3(1,1,1),
                                            32.0f));
        mesh1->setMaterial(new PhongMaterial(Vector3(0,0,0.75),
                                             Vector3(0,0,1),
                                             Vector3(0.5,0.5,1),
                                             16.0f));
        
        m.scene_->addObject(mesh);
        m.scene_->addObject(mesh1);
        
        m.scene_->addLight(l1);
        m.scene_->ambientColor = Vector3(1,1,1) * 0.2f;
    
    }else if (name == "D1")
    {
        Octree * ot = new Octree();
         
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                for (int k = 0; k < 10; k++) {
                    Sphere * s = new Sphere(Vector3(-4.5f+i,
                                                    -4.5f+j,
                                                    -(k*k*k)),
                                            0.25f);
                    s->setMaterial(new PhongMaterial(Vector3(0,0,0.75),
                                                     Vector3(0,0,1),
                                                     Vector3(0.5,0.5,1),16.0f));
                    ot->addObject(s);
                }
            }
        }
        
        ot->build();
        std::cout << "done building octree\n";
        m.scene_->addObject(ot);
        m.scene_->addLight(l1);
        m.scene_->ambientColor = Vector3(1,1,1) * 0.2f;
        
    }else if(name == "D2"){
        // Simple scene with spheres and area light
        s->setMaterial(new PhongMaterial(Vector3(0.75,0,0),
                                         Vector3(1,0,0),
                                         Vector3(1,1,1),
                                         32.0f));
        s1->setMaterial(new PhongMaterial(Vector3(0,0,0.75),
                                          Vector3(0,0,1),
                                          Vector3(0.5,0.5,1),
                                          16.0f));
        
        m.scene_->addObject(s);
        m.scene_->addObject(s1);
        
        DiskLight * l = new DiskLight(Vector3(10,10,10),
                                      Vector3(0,0,0)-Vector3(10,10,10),
                                      1.0f,
                                      Vector3(1.0f,1.0f,1.0f),
                                      0, 1, 1);
        m.scene_->addObject(l);
        m.scene_->addLight(l);
        
        m.scene_->ambientColor = Vector3(1,1,1) * 0.2f;
        
    }else{
        std::cout << "Module " << name << " is not defined. Exiting!\n";
        exit(EXIT_FAILURE);
    }
    
    return m;
}
