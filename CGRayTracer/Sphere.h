//
//  Sphere.h
//  CGRayTracer
//
//  Created by Nicholas Pleschko on 9/23/12.
//  Copyright (c) 2012 Nicholas Pleschko. All rights reserved.
//

#ifndef __CGRayTracer__Sphere__
#define __CGRayTracer__Sphere__

#include <iostream>
#include "SceneObject.h"
#include "utils.h"
#include "Ray.h"

class Sphere : public SceneObject {
    
    Vector3 center_;
    double radius_;
    Vector3 northpole_;
    Vector3 primedir_;
    Vector3 seconddir_;
    
    AABoundingBox bounds_;
    
public:
    Sphere(Vector3 center, double radius):SceneObject(),center_(center),radius_(radius){
        bounds_.center = center;
        bounds_.min = center - Vector3(radius,radius,radius);
        bounds_.max = center + Vector3(radius,radius,radius);
        bounds_.updateCorners();
        setNorthpole(Vector3(0,1,0), Vector3(1,0,0));
    };
    virtual IntersectionPoint intersect(Ray &r);
    
    AABoundingBox boundingBox(){
        return bounds_;
    };
    
    bool intersections(Ray& r, double& t0, double& t1);
    bool insideSphere(Vector3 p);
    Vector3 normalAtPoint(Vector3 p);
    void setNorthpole(Vector3 north, Vector3 prime);
    
};

#endif /* defined(__CGRayTracer__Sphere__) */
